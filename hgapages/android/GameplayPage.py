from appium.webdriver.common.touch_action import TouchAction
from hgapages.android import HomeScreenPage as hsp
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
import time

class GameplayPage(hsp.HomeScreenPage):

    # Elements on On Turn page
    raise_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.Button[contains(@resource-id,'button')]")
    raise_bet_button_label_text = (MobileBy.ID, 'textViewLabel1')
    raise_bet_button_label_number = (MobileBy.ID, 'textViewLabel2')
    call_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1][contains(@resource-id,'textViewLabel1') and @text='CALL']")
    call_button_label = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]")
    check_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.Button[contains(@resource-id,'button') and @text='CHECK']")
    fold_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[6][contains(@resource-id,'buttonNotOnTurnAction02') and @text='FOLD']")
    bet_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.Button[contains(@resource-id,'button')]")
    raise_2bb_button = (MobileBy.ID, 'buttonPlay01')
    raise_3bb_button = (MobileBy.ID, 'buttonPlay02')
    raise_4bb_button = (MobileBy.ID, 'buttonPlay03')
    all_in_button = (MobileBy.ID, 'buttonPlayALLIN')
    raise_3dots_button = (MobileBy.ID, 'buttonPlay04')
    minus_button = (MobileBy.ID, 'buttonPlayMinus')
    plus_button = (MobileBy.ID, 'buttonPlayPlus')
    dealer_info = (MobileBy.ID, 'isDealer')
    amount_on_the_button = (MobileBy.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[2][contains(@resource-id,'textViewLabel2')]")
    current_amount = (MobileBy.ID, 'toolbarPlayerLabel')
    picker_list_BB = (MobileBy.ID, 'lblBetValueInBB')
    picker_list_value = (MobileBy.ID, 'lblBetValue')
    gameplay_page_text = (MobileBy.ID, 'textViewContent')
    folded_page_image = (MobileBy.ID, 'imageViewWait')
    folded_page_subtext = (MobileBy.ID, 'textViewAddendum')
    win_screen_image = (MobileBy.ID, 'imageViewCelebration')
    win_screen_button = (MobileBy.ID, 'buttonWaitAction02')
    bb_value = (MobileBy.ID, 'bigBlind')
    sb_value = (MobileBy.ID, 'smallBlind')

    # Elements on Not on Turn page
    tease_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[1][contains(@resource-id,'buttonNotOnTurnTease') and @text='TEASE']")
    call_any_advance_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[3][contains(@resource-id,'buttonNotOnTurnAction01') and @text='CALL ANY']")
    check_advance_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[2][contains(@resource-id,'buttonNotOnTurnAction00') and @text='CHECK']")
    check_fold_advance_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[4][contains(@resource-id,'buttonNotOnTurnAction02') and @text='CHECK/FOLD']")

    # ____________________________Helper Methods ___________________________________

    # function that returns if player is dealer or not
    def is_dealer(self):
        element = self.find_element(self.dealer_info)
        return element.get_attribute('text')

    # Method returns amount shown on the button
    # label - lebel from the button that contains number
    def get_amount_from_the_button(self, label):
        log.logging.info( "Amount from the button is: {}".format(label.get_attribute('text')))
        return label.get_attribute('text')

    # Method opens picker wheel
    def open_picker_wheel(self):
        log.logging.info("Opening picker wheel")
        self.click_on_element(self.raise_3dots_button)


    # Scroll picker list with desired number of lines
    def scroll_picker_list(self, scroll_lines=None):
        list_of_all_pickers = self.find_element(self.picker_list_BB, find_all_elements=True)
        zero_picker = list_of_all_pickers[0]
        desired_picker = list_of_all_pickers[scroll_lines]
        action = TouchAction(self.driver)
        action.press(zero_picker).wait(1500).move_to(desired_picker).release().perform()

    # Scroll picker list to the upper element - ALL IN
    # In both QA tables (40 and 70 min amount) it requires 2 full scroll for reaching the top option

    def scroll_picker_list_to_all_in(self):
        for loop in range(2):
            self.scroll_picker_list(scroll_lines=-1)

    # Method returns value from picker list
    def get_picker_value(self, index_value=None):
        picker_list_with_values = self.find_element(self.picker_list_value, find_all_elements=True)
        return picker_list_with_values[index_value].get_attribute("text")

    def get_picker_list_with_values(self):
        picker_list_textual = self.list_to_textual_list(self.find_element(self.picker_list_value, find_all_elements=True))
        return picker_list_textual[1:-1]

    def convert_picker_list_with_values_to_float(self, list):
        new_list = []
        for x in list:
            new_list.append(float(x))
        return new_list

    # Method returns list based on BB
    # i.e . bb -2  => list is: [20.0, 40.0, 60.0]
    # Note - list of float values

    def create_list_based_on_BB(self, BB):
        list = [10, 20, 30]
        desired_list = []
        for x in list:
            value = x * float(BB)
            desired_list.append(value)
        return desired_list

    def picker_list_values_snown_properly(self, picker_list_with_values, list_based_on_BB):
        if picker_list_with_values == list_based_on_BB:
            log.logging.info('Picker list values are shown properly ')
            return True
        else:
            log.logging.info('Picker list values are not shown properly ')
            return False

    def return_pickerBB_list(self):
        list = self.find_element(self.picker_list_BB, find_all_elements=True)
        return list


    # Method that returns TRUE if 10 BB, 20 BB, 30 BB are shown properly > (30 BB)
    def picker_list_BB_shown_properly(self, list):
        # picker_list_textual = self.list_to_textual_list(list)
        if all(x in list for x in ['(10 BB)', '(20 BB)', '(30 BB)']):
            return True

    # Method returns True if button is shown and has correct name
    # Arguments:
    # button_label - button label you want to check define in GameplayPage. If button is contained from two labels as Raise, Bet use defined one (for example, raise_bet_button_label_text) otherwise use defined button
    # button_text  - text that is shown in app

    def button_is_shown(self, button_label, button_text):
        value = button_label.is_displayed()
        text = button_label.get_attribute('text')
        if (button_text == text) and value:
            log.logging.info('{} is shown properly'.format(button_text))
            return True

        else:
            log.logging.info('{} is not shown properly'.format(button_text))
            return False

    def get_folded_page_text(self):
        element = self.find_element(self.gameplay_page_text)
        log.logging.info('Folded page text is: {}'.format(element.get_attribute('text')))
        return element.get_attribute('text')


    def get_folded_page_subtext(self):
        element = self.find_element(self.folded_page_subtext)
        log.logging.info('Folded page subtext is: {}'.format(element.get_attribute('text')))
        return element.get_attribute('text')

    def get_win_screen_text(self):
        element = self.find_element(self.win_screen_button)
        log.logging.info('Win screen text is: {}'.format(element.get_attribute('text')))
        return element.get_attribute('text')


    def get_bb_value(self):
        element = self.find_element(self.bb_value)
        log.logging.info('BB is: {}'.format(element.get_attribute('text')))
        return element.get_attribute('text')

    def get_sb_value(self):
        element = self.find_element(self.sb_value)
        log.logging.info('SB is: {}'.format(element.get_attribute('text')))
        return element.get_attribute('text')


    #-------------------------------------------Action Method--------------------------------------------------------


    def check_who_is_dealer(self, gameplay_page, gameplay_page2):
        if gameplay_page.is_dealer() == "true":
            dealer = gameplay_page
            non_dealer = gameplay_page2
        else:
            dealer = gameplay_page2
            non_dealer = gameplay_page
        return [dealer, non_dealer]

    def check_who_is_dealer_for_settings(self, gameplay_page, settings_page, setting_page2):
        if gameplay_page.is_dealer() == "true":
            dealer = settings_page
            non_dealer = setting_page2
        else:
            dealer = setting_page2
            non_dealer = settings_page
        return [dealer, non_dealer]

    def play_for_call_any_as_all_in_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.call_any_advance_button)
        dealer.click_on_element(self.raise_3dots_button)
        dealer.scroll_picker_list_to_all_in()
        # Moving focus - closing picker wheel
        dealer.click_on_element(self.raise_button)
        time.sleep(2)
        dealer.click_on_element(self.raise_button)
        time.sleep(2.5)

    def play_for_call_any_as_call_test(self, dealer, non_dealer):
        BALANCE_INFO_FIRST = dealer.get_current_amount()
        SB_AMOUNT = dealer.get_sb_value()
        dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_any_advance_button)
        time.sleep(2.5)
        AMOUNT_OF_RAISE = non_dealer.get_amount_from_the_button(non_dealer.find_element(self.raise_bet_button_label_number))
        non_dealer.click_on_element(self.raise_button)
        time.sleep(5)
        BALANCE_INFO_SECOND = dealer.get_current_amount()
        dealer.click_on_element(self.call_any_advance_button)
        non_dealer.click_on_element(self.check_button)
        time.sleep(3)
        non_dealer.click_on_element(self.bet_button)
        dealer.click_on_element(self.call_button)
        non_dealer.click_on_element(self.check_button)
        dealer.click_on_element(self.check_button)
        return [BALANCE_INFO_FIRST, SB_AMOUNT, BALANCE_INFO_SECOND,AMOUNT_OF_RAISE]

    def play_for_call_any_as_check_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_any_advance_button)
        non_dealer.click_on_element(self.raise_button)
        time.sleep(5)
        BALANCE_INFO_FIRST = dealer.get_current_amount()
        dealer.click_on_element(self.call_any_advance_button)
        non_dealer.click_on_element(self.check_button)
        time.sleep(4.5)
        non_dealer.click_on_element(self.bet_button)
        BALANCE_INFO_SECOND = dealer.get_current_amount()
        dealer.click_on_element(self.call_button)
        non_dealer.click_on_element(self.check_button)
        dealer.click_on_element(self.check_button)
        return [BALANCE_INFO_FIRST, BALANCE_INFO_SECOND]

    def play_for_check_fold_as_check_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        BALANCE_FIRST = dealer.get_current_amount()
        dealer.click_on_element(self.check_fold_advance_button)
        non_dealer.click_on_element(self.call_button)
        non_dealer.click_on_element(self.check_button)
        BALANCE_SECOND = dealer.get_current_amount()
        return [BALANCE_FIRST,BALANCE_SECOND]

    def play_for_check_fold_as_fold_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.check_fold_advance_button)
        PLAYER_BALANCE_BEFORE_FOLD = dealer.get_current_amount()
        non_dealer.click_on_element(self.raise_button)
        time.sleep(6)
        if dealer.button_is_shown(self.find_element(self.check_advance_button), 'CHECK'):
            PLAYER_BALANCE_AFTER_FOLD = dealer.get_current_amount()
        return [PLAYER_BALANCE_BEFORE_FOLD,PLAYER_BALANCE_AFTER_FOLD]

    def play_for_check_NAPPS_678_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.call_button)
        time.sleep(4.5)
        BALANCE_BEFORE = dealer.get_current_amount()
        dealer.click_on_element(self.check_advance_button)
        non_dealer.click_on_element(self.check_button)
        time.sleep(3)
        non_dealer.click_on_element(self.bet_button)
        non_dealer.click_on_element(self.check_advance_button)
        BALANCE_AFTER = dealer.get_current_amount()
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.call_button)
        # non_dealer.click_on_element(self.check_button)
        dealer.click_on_element(self.check_button)
        return [BALANCE_BEFORE,BALANCE_AFTER ]

    def play_for_win_screen_test(self, dealer, non_dealer):
        dealer.click_on_element(self.fold_button)
        non_dealer.click_on_element(self.win_screen_button)
        win_screen_text = non_dealer.get_win_screen_text()
        return  win_screen_text

    def before_bring_more_chips_gameplay(self, dealer, non_dealer):
        amount_before_bringing_more_chips = non_dealer.get_current_amount()
        BB = non_dealer.get_bb_value()
        sum = float(BB) + float(amount_before_bringing_more_chips)
        log.logging.info('Amount after bringing more chips: {}'.format(sum))
        return float(sum)

    def bring_more_chips_gameplay(self, dealer, non_dealer):
        dealer.click_on_element(self.fold_button)
        if non_dealer.button_is_shown(self.find_element(self.plus_button), "+"):
            time.sleep(8)
            amount_after_bringing_more_chips = non_dealer.get_current_amount()
        log.logging.info('Amount after bringing more chips: {}'.format(amount_after_bringing_more_chips))
        return float(amount_after_bringing_more_chips)

    def play_for_high_bet_mechanism(self):
        current_button_amount = self.get_amount_from_the_button(self.find_element(self.raise_bet_button_label_number))
        self.click_on_element(self.raise_3dots_button)
        self.scroll_picker_list(2)
        log.logging.info('Amount: {}'.format(current_button_amount))
        lowest_picker_value = self.get_picker_value(-1)
        log.logging.info('Lowest picker value: {}'.format(lowest_picker_value))
        current_amount = self.get_current_amount()
        log.logging.info('Total amount: {}'.format(current_amount))
        all_in_picker_value = self.get_picker_value(0)
        log.logging.info('All in picker value: {}'.format(all_in_picker_value))
        bb = self.get_bb_value()
        list = self.get_picker_list_with_values()
        list_float = self.convert_picker_list_with_values_to_float(list[::-1])
        desired_list = self.create_list_based_on_BB(bb)
        picker_bb_list= self.list_to_textual_list(self.return_pickerBB_list())
        self.click_on_element(self.raise_3dots_button)
        return [float(current_amount), float(current_button_amount), float(lowest_picker_value), float(all_in_picker_value),list_float, desired_list, picker_bb_list]


    def accelerator_buttons_are_shown_properly(self):
        bb3_is_shown = self.button_is_shown(self.find_element(self.raise_3bb_button),'3 BB')
        bb4_is_shown = self.button_is_shown(self.find_element(self.raise_4bb_button),'4 BB')
        dots_are_shown = self.button_is_shown(self.find_element(self.raise_3dots_button),'...')
        all_in_button_is_shown = self.button_is_shown(self.find_element(self.all_in_button),'ALL IN')
        value = bb3_is_shown and bb4_is_shown and dots_are_shown and all_in_button_is_shown
        if value:
            log.logging.info('Accelerator buttons are shown properly')
            return True
        else:
            log.logging.info('Accelerator buttons are not shown properly')
            return False

    def play_for_NAPPS_1827(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        time.sleep(2)
        raise_button_value_before = float(non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_bet_button_label_number)))
        log.logging.info('Raise button value before: {}'.format(raise_button_value_before))
        non_dealer.click_on_element(non_dealer.raise_3bb_button)
        raise_button_value_after = non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_bet_button_label_number))
        log.logging.info('Raise button value after: {}'.format(raise_button_value_after))
        return [raise_button_value_before, raise_button_value_after]

    def play_for_NAPPS_1828(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        time.sleep(2)
        raise_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_bet_button_label_number)))
        log.logging.info('Raise button value before: {}'.format(raise_button_value_before))
        non_dealer.click_on_element(non_dealer.raise_4bb_button)
        raise_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.raise_bet_button_label_number))
        log.logging.info('Raise button value after: {}'.format(raise_button_value_after))
        return [raise_button_value_before, raise_button_value_after]

    def play_for_NAPPS_1833(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        time.sleep(2)
        raise_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_bet_button_label_number)))
        log.logging.info('Raise button value before: {}'.format(raise_button_value_before))
        non_dealer.click_on_element(non_dealer.all_in_button)
        raise_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.raise_bet_button_label_number))
        log.logging.info('Raise button value after: {}'.format(raise_button_value_after))
        return [raise_button_value_before, raise_button_value_after]

    def play_for_NAPPS_1837(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_button)
        time.sleep(2)
        bet_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_bet_button_label_number)))
        log.logging.info('Bet button value before: {}'.format(bet_button_value_before))
        non_dealer.click_on_element(non_dealer.raise_3bb_button)
        bet_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.raise_bet_button_label_number))
        log.logging.info('Bet button value after: {}'.format(bet_button_value_after))
        return [bet_button_value_before, bet_button_value_after]

    def play_for_NAPPS_1838(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_button)
        time.sleep(2)
        bet_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_bet_button_label_number)))
        log.logging.info('Bet button value before: {}'.format(bet_button_value_before))
        non_dealer.click_on_element(non_dealer.raise_4bb_button)
        bet_button_value_after = non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_bet_button_label_number))
        log.logging.info('Bet button value after: {}'.format(bet_button_value_after))
        return [bet_button_value_before, bet_button_value_after]

    def play_for_NAPPS_1840(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_button)
        time.sleep(2)
        bet_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.bet_button)))
        log.logging.info('Bet button value before: {}'.format(bet_button_value_before))
        non_dealer.click_on_element(non_dealer.all_in_button)
        bet_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.bet_button))
        log.logging.info('Bet button value after: {}'.format(bet_button_value_after))
        return [bet_button_value_before,bet_button_value_after]