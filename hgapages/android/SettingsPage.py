from hgapages.android import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log

class SettingsPage(HomeScreenPage.HomeScreenPage):

    # Elements on Settings page - not in game
    sounds_title = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.TextView[1]')
    sounds_description = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.TextView[2]')
    sounds_toggle_button = (MobileBy.ID, 'switchButton')
    about_title = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.TextView[1]')
    about_description = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.TextView[2]')
    logOut_title = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.TextView[1]')
    logOut_description = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.TextView[2]')

    #Elements on About Page
    about_redLark = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.TextView[1]')
    terms_and_conditions = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.TextView[1]')
    privacy_policy = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.TextView[1]')
    app_version = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.TextView[1]')
    version_info =(MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.TextView[2]')

    # Elements on Game option (Settings page) - in game

    close_X_button = (MobileBy.ID, 'rightIcon')
    table_info_button = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.TextView[1]')
    table_info_description = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.TextView[2]')
    bring_more_chips_button = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.TextView[1]')
    bring_more_chips_description = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.TextView[2]')
    sit_out_switch = (MobileBy.ID, 'switchButton')
    sit_out_button = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.TextView[1]')
    sit_out_description = (MobileBy.XPATH, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.TextView[2]')
    leave_game_button = (MobileBy.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.TextView[1][contains(@resource-id,'headline') and @text='Leave game']")
    leave_game_description = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.TextView[2]")
    leave_game_pop_up_description = (MobileBy.ID, 'messageDescription')
    leave_game_pop_up_cancel = (MobileBy.ID, 'cancel')
    leave_table_popup_button = (MobileBy.ID, 'confirm')

    # Leaderboard page
    back_arrow_button = (MobileBy.ID, 'leftIcon')
    sitting_out_info = (MobileBy.ID, 'out')
    i_m_back_button = (MobileBy.XPATH, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[contains(@resource-id,'buttonWaitAction02') and contains(@text, 'BACK')]")

    # Bring more chips page
    bring_more_chips_title = (MobileBy.ID, 'title')
    in_wallet_text = (MobileBy.ID, 'textInWalletLabel')
    in_wallet_amount = (MobileBy.ID, 'textInWalletValue')
    bring_in_input_field = (MobileBy.ID, 'editBuyInValue')
    bring_in_slider = (MobileBy.ID, 'seekBarBuy')
    bring_in_minimum_amount = (MobileBy.ID, 'textSeekMinLabel')
    bring_in_maximum_amount = (MobileBy.ID, 'textSeekMaxLabel')
    bring_money_button = (MobileBy.ID, 'buttonBottomRight')
    cancel_button_bring_more_chips = (MobileBy.ID, 'buttonBottomLeft')

    # pop-up (bring more chips)
    pop_up_text_bring_in = (MobileBy.ID, 'messageDescription')
    yes_button_bring_in = (MobileBy.ID, 'confirm')
    cancel_button_for_bring_in = (MobileBy.ID, 'cancel')

    #---------------------------------- Methods---------------------------------------

    def sit_out_player(self):
        log.logging.info('Siting out player')
        self.click_on_element(self.settings_button)
        self.click_on_element(self.sit_out_switch)
        self.click_on_element(self.close_X_button)


    def leave_game_player(self):
        log.logging.info('Leaving the game')
        self.click_on_element(self.settings_button)
        self.click_on_element(self.leave_game_button)
        self.click_on_element(self.leave_table_popup_button)

    def settings_page_is_shown_properly(self):
        sounds_is_shown_properly = self.label_is_shown(self.sounds_title, "Sounds")
        sounds_description_is_shown_properly = self.label_is_shown(self.sounds_description,'Sounds are on')
        about_is_shown_proprly = self.label_is_shown(self.about_title, "About")
        about_description_is_shown_properly = self.label_is_shown(self.about_description, 'More about the app, terms of service and privacy policy')
        log_out_is_shown_properly = self.label_is_shown(self.logOut_title,'Log out')
        log_out_description_is_shown_properly = self.label_is_shown(self.logOut_description, '… and make sure to come back soon!')
        value = self.find_element(self.sounds_toggle_button).is_displayed()
        if sounds_is_shown_properly and sounds_description_is_shown_properly and about_is_shown_proprly and about_description_is_shown_properly and log_out_is_shown_properly and log_out_description_is_shown_properly and value:
            log.logging.info('Settings page is shown properly')
            return True
        else:
            log.logging.info('Settings page is not shown properly')
            return False

    def about_page_is_shown_properly(self):
        about_redLark_is_shown_propelry = self.label_is_shown(self.about_redLark, 'About RedLark')
        terms_and_conditions_is_shown_properly = self.label_is_shown(self.terms_and_conditions, 'Terms & conditions')
        privacy_policy_is_shown_properly = self.label_is_shown(self.privacy_policy, 'Privacy policy')
        app_version_is_shown_properly = self.label_is_shown(self.app_version, 'App version')
        # build_version_is_shown_properly = self.label_is_shown(self.version_info, '{}'.format(build_number))
        if about_redLark_is_shown_propelry and terms_and_conditions_is_shown_properly and privacy_policy_is_shown_properly:
            log.logging.info('About page is shown properly')
            return True
        else:
            log.logging.info('About page is not shown properly')
            return False

    def game_options_page_is_shown_properly(self):
        table_info_shown_properly = self.label_is_shown(self.table_info_button, "Table info")
        table_info_description_is_shown_properly = self.label_is_shown(self.table_info_description, 'See useful information about table')
        bring_more_chips_shown_proprly = self.label_is_shown(self.bring_more_chips_button, "Bring more chips")
        bring_more_chips_description_is_shown_properly = self.label_is_shown(self.bring_more_chips_description, 'Get some fresh chips in the game')
        sitout_shown_properly = self.label_is_shown(self.sit_out_button, "Sit out")
        sitout_description_shown_properly = self.label_is_shown(self.sit_out_description, "Take a break for new hands")
        leave_game_shown_properly = self.label_is_shown(self.leave_game_button, "Leave game")
        leave_game_description_shown_properly = self.label_is_shown(self.leave_game_description, "Stand out and leave the current game")
        value = self.find_element(self.sit_out_switch).is_displayed()
        if table_info_shown_properly and table_info_description_is_shown_properly and bring_more_chips_shown_proprly and bring_more_chips_description_is_shown_properly and \
                sitout_shown_properly and sitout_description_shown_properly and leave_game_shown_properly and leave_game_description_shown_properly and value:
            log.logging.info('Game options page is shown properly')
            return True
        else:
            log.logging.info('Game options page is not shown properly')
            return False

    def leave_game_page_is_shown_properly(self):
        desciption_shown_properly = self.label_is_shown(self.leave_game_pop_up_description, 'You will be automatically folded if you leave the table.')
        cancel_buton_is_shown_properly = self.button_is_shown(self.find_element(self.leave_game_pop_up_cancel),'CANCEL')
        leave_buton_is_shown_properly = self.button_is_shown(self.find_element(self.leave_table_popup_button),'LEAVE TABLE')
        value = desciption_shown_properly and cancel_buton_is_shown_properly and leave_buton_is_shown_properly
        if value:
            log.logging.info('Leave game page is shown properly')
            return True
        else:
            log.logging.info('Leave game page is not shown properly')
            return False

    def bring_more_chips(self, chips_amount = None):
        self.click_on_element(self.settings_button)
        self.click_on_element(self.bring_more_chips_button)
        self.click_on_element(self.bring_in_input_field)
        self.find_element(self.bring_in_input_field).clear()
        if chips_amount is None:
            minimum = self.find_element(self.bring_in_minimum_amount).get_attribute('text')
            self.chips_amount = minimum
        else:
            self.chips_amount = chips_amount
        self.find_element(self.bring_in_input_field).send_keys(self.chips_amount)
        self.click_on_element(self.bring_money_button)
        self.click_on_element(self.yes_button_bring_in)
        log.logging.info('Bring more chips added, amount:{}'.format(self.chips_amount))