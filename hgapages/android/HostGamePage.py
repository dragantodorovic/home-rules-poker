from appium.webdriver.common.touch_action import TouchAction
from hgapages.android import HomeScreenPage as hsp
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
import time
import re


class HostGamePage(hsp.HomeScreenPage):
    # Elements on host a game page
    back_button = (MobileBy.ID, 'buttonBottomLeft')
    create_table_button = (MobileBy.ID, 'buttonBottomRight')
    pick_your_bring_in = (MobileBy.ID, 'tvPickYourBringIn')
    bring_in_25BB = (MobileBy.XPATH,
                     '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[3]/android.view.ViewGroup[1]/android.widget.TextView')
    bring_in_50BB = (MobileBy.XPATH,
                     '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[3]/android.view.ViewGroup[2]/android.widget.TextView')
    bring_in_75BB = (MobileBy.XPATH,
                     '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[3]/android.view.ViewGroup[3]/android.widget.TextView')
    bring_in_100BB = (MobileBy.XPATH,
                      '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[3]/android.view.ViewGroup[4]/android.widget.TextView')
    pick_your_blinds_text = (MobileBy.XPATH,
                             '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView[2]')
    blinds_1_2 = (MobileBy.XPATH,
                  '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[2]/android.view.ViewGroup[1]/android.widget.TextView')
    blinds_2_4 = (MobileBy.XPATH,
                  '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[2]/android.view.ViewGroup[2]/android.widget.TextView')
    blinds_5_10 = (MobileBy.XPATH,
                   '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[2]/android.view.ViewGroup[3]/android.widget.TextView')
    blinds_10_20 = (MobileBy.XPATH,
                    '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[2]/android.view.ViewGroup[4]/android.widget.TextView')
    blinds_25_50 = (MobileBy.XPATH,
                    'hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[2]/android.view.ViewGroup[5]/android.widget.TextView')
    choose_number_of_seats_text = (MobileBy.ID, 'tvChooseNumOfSeats:')
    number_of_seats_2 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[1]/android.widget.TextView')
    number_of_seats_3 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[2]/android.widget.TextView')
    number_of_seats_4 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[3]/android.widget.TextView')
    number_of_seats_5 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[4]/android.widget.TextView')
    number_of_seats_6 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[5]/android.widget.TextView')
    number_of_seats_7 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[6]/android.widget.TextView')
    number_of_seats_8 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[7]/android.widget.TextView')
    number_of_seats_9 = (MobileBy.XPATH,
                         '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[8]/android.widget.TextView')

    # Elements on create table page
    cancel_button = (MobileBy.ID, 'cancel')
    retry_button = (MobileBy.ID, 'confirm')
    unable_to_host_such_table_text = (MobileBy.ID, 'messageDescription')
    table_device_link = (MobileBy.ID, 'tvTableLink')
    table_device_link_text = (MobileBy.ID, 'tvTableLinkText')
    player_device_code_text = (MobileBy.ID, 'tvTableIdText')
    you_are_all_set_text = (MobileBy.ID, 'tvAllSet')
    player_device_code = (MobileBy.ID, 'tvTableId')
    ok_button = (MobileBy.ID, 'buttonOk')

    # ---------------------------------Methods-----------------------------

    def set_up_nonexistent_env(self):
        log.logging.info('Setting up environment for nonexistent flow')
        self.set_up_table(9,50,100)

    def set_up_existent_env(self):
        log.logging.info('Setting up environment for existent flow')
        [number_of_seats, values_of_blinds, values_of_bring_in] = self.set_up_table(3, 2, 25)
        return [number_of_seats, values_of_blinds, values_of_bring_in]

    def non_existent_dialog_is_shown_properly(self):
        log.logging.info('Verifying pop up dialog is shown properly:')
        pop_up_dialog_text = self.find_element(self.unable_to_host_such_table_text).get_attribute('text')
        cancel_button = self.find_element(self.cancel_button)
        try_again = self.find_element(self.retry_button)
        cancel_button_is_shown = self.button_is_shown(cancel_button, 'CANCEL')
        try_again_button_is_shown = self.button_is_shown(try_again, 'RETRY')
        if pop_up_dialog_text == 'Unable to sit at the table':
            value = True
            log.logging.info('Pop up dialog text is shown properly')
        else:
            value = False
        if cancel_button_is_shown and try_again_button_is_shown and value:
            return True
        else:
            False

    def table_info_screen_is_shown_properly(self):
        log.logging.info('Verifying that table info screen is shown properly:')
        you_are_all_set_text = self.find_element(self.you_are_all_set_text).get_attribute('text')
        table_device_link_text = self.find_element(self.table_device_link_text).get_attribute('text')
        player_device_code_text = self.find_element(self.player_device_code_text).get_attribute('text')
        ok_button = self.find_element(self.ok_button)
        ok_button_is_shown = self.button_is_shown(ok_button, 'OK')
        if you_are_all_set_text == "You are all set!" and table_device_link_text == "Table device link:" and player_device_code_text == "Player device code:":
            value = True
            log.logging.info('Table info screen text is shown properly')
        else:
            value = False
        if ok_button_is_shown and value:
            return True
        else:
            False

    def host_a_game_nonexistent_flow_cancel_scenario(self):
        self.click_on_element(self.host_a_game_button)
        self.set_up_nonexistent_env()
        log.logging.info('Clicking on create table button')
        log.logging.info('Clicking on cancel button')
        self.click_on_element(self.cancel_button)
        element = self.find_element(self.pick_your_blinds_text)
        pick_your_blinds_text = element.get_attribute('text')
        return pick_your_blinds_text

    def host_a_game_nonexistent_flow_try_again_scenario(self):
        self.click_on_element(self.host_a_game_button)
        self.set_up_nonexistent_env()
        log.logging.info('Clicking on create table button')
        #self.non_existent_dialog_is_shown_properly()
        log.logging.info('Clicking on try again button')
        self.click_on_element(self.retry_button)
        value = self.non_existent_dialog_is_shown_properly()
        return value

    def host_a_game_existent_flow_table_info_screen(self):
        self.click_on_element(self.host_a_game_button)
        self.set_up_existent_env()
        log.logging.info('Clicking on create table button')
        value = self.table_info_screen_is_shown_properly()
        return value

    def host_a_game_existent_flow_web_validation(self):
        self.click_on_element(self.host_a_game_button)
        [number_of_seats, values_of_blinds, values_of_bring_in] = self.set_up_existent_env()
        log.logging.info('Clicking on create table button')
        device_code = self.find_element(self.player_device_code).get_attribute('text')
        self.click_on_element(self.ok_button)
        return [number_of_seats, values_of_blinds, values_of_bring_in, device_code]

    def set_up_table(self, number_of_seats=None, values_of_blinds=None, values_of_bring_in=None):
        switcher_number_of_seats = {
            2: self.number_of_seats_2,
            3: self.number_of_seats_3,
            4: self.number_of_seats_4,
            5: self.number_of_seats_5,
            6: self.number_of_seats_6,
            7: self.number_of_seats_7,
            8: self.number_of_seats_8,
            9: self.number_of_seats_9
        }
        chosen_seats = switcher_number_of_seats.get(number_of_seats, self.number_of_seats_2)

        switcher_value_of_blinds = {
            2: self.blinds_1_2,
            4: self.blinds_2_4,
            10: self.blinds_5_10,
            20: self.blinds_10_20,
            50: self.blinds_25_50
        }
        chosen_value_of_blinds = switcher_value_of_blinds.get(values_of_blinds, self.blinds_1_2)

        switcher_values_of_bring_in = {
            25: self.bring_in_25BB,
            50: self.bring_in_50BB,
            75: self.bring_in_75BB,
            100: self.bring_in_100BB
        }
        chosen_values_of_bring_in = switcher_values_of_bring_in.get(values_of_bring_in, self.bring_in_25BB)

        self.click_on_element(chosen_seats)
        self.click_on_element(chosen_value_of_blinds)
        self.click_on_element(chosen_values_of_bring_in)
        self.click_on_element(self.create_table_button)

        return [number_of_seats, values_of_blinds, values_of_bring_in]
