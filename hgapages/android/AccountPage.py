from hgaconfig.Constants import *
from hgapages.android import LaunchScreenPage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log



class AccountPage(LaunchScreenPage.LaunchScreenPage):

    # Elements on sign in page
    username_field = (MobileBy.ID, 'igsetUserName')
    password_field = (MobileBy.ID, 'igsetPassword')
    login_button = (MobileBy.ID, 'buttonLogin')
    show_password_button = (MobileBy.ID, 'text_input_password_toggle')


    #___________________ Methods_______________________________________

    def login_user(self, name=None):
        if name is None:
            self.name = Constants.account_username
        else:
            self.name = name
        self.click_on_element(self.login_button_launch_screen)
        self.click_on_element(self.username_field)
        self.find_element(self.username_field).clear()
        log.logging.info('Entering username: {}'.format(name))
        self.find_element(self.username_field).send_keys(self.name)
        self.click_on_element(self.password_field)
        self.find_element(self.password_field).clear()
        log.logging.info('Entering password: {} '.format(Constants.account_password))
        self.find_element(self.password_field).send_keys(Constants.account_password)
        # self.driver.hide_keyboard()
        self.hide_keyboard()
        self.click_on_element(self.login_button)

    def login_guest(self):
        self.click_on_element(self.play_as_guest_button)
