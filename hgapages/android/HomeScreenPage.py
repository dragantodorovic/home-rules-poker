import time
from hgaconfig.Constants import *
from hgapages.android import BasePage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log




class HomeScreenPage(BasePage.BasePage):

    # Elements on Welcome back page
    join_a_game_button = (MobileBy.ID, 'buttonJoin')
    host_a_game_button = (MobileBy.ID, 'buttonHost')
    shop_button = (MobileBy.ID, 'buttonShop')
    preview_banner_button = (MobileBy.ID, 'buttonBanner')
    settings_button = (MobileBy.ID, 'toolbarImageSettings')
    avatar_image = (MobileBy.ID, 'toolbarImageAvatar')
    nickname_info = (MobileBy.ID, 'toolbarPlayerLabel')
    balance_info = (MobileBy.ID, 'toolbarPlayerBalance')
    # balance_info = (MobileBy.ID, 'toolbarPlayerLabel')
    setings_tab = (MobileBy.ID, 'Settings')

    # Buy-in page
    minus_button = (MobileBy.ID, 'buttonMinus')
    plus_button = (MobileBy.ID, 'buttonPlus')
    bring_in_amount_info = (MobileBy.ID, 'textViewBuyInValue')
    amount_slider = (MobileBy.ID, 'seekBarCustom')
    cancel_button = (MobileBy.ID, 'buttonBottomLeft')
    join_game_button = (MobileBy.ID, 'buttonBottomRight')

    # Pre-Gameplay page
    post_bb_button = (MobileBy.ID, 'buttonActionSingle')

    #_____________Methods_____________________________________


    # Shows amount for bring in field
    def get_bring_in_amount(self):
        element = self.find_element(locator=self.bring_in_amount_info)
        return element.get_attribute("value")

    # Get total amount
    def get_current_amount(self):
        element = self.find_element(locator=self.balance_info)
        log.logging.info('Current balance is:{}'.format(element.get_attribute("text")))
        return element.get_attribute("text")


    def list_to_textual_list(self,list_of_elements):
        textual_list = []
        for element in list_of_elements:
            textual_list.append(element.get_attribute("text"))
        return textual_list