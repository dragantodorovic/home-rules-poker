from hgapages.android import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy
import time
from hgaconfig import LoggingConfig as log

class AccountSettingsPage(HomeScreenPage.HomeScreenPage):

    # Elements on My Profile page
    my_profile_tab = (MobileBy.ID, 'tabMyProfile')
    settings_tab = (MobileBy.ID, 'tabSettings')
    avatar_view_image = (MobileBy.ID, 'imageViewAvatar')
    nickname_text = (MobileBy.ID, 'textViewNick')

    # Elements on Settings page
    logout_button = (MobileBy.ID, 'logOut')

    # _________________METHODS_________________________________

    def logout_user(self):
        time.sleep(3)
        log.logging.info('Player is logging out')
        self.click_on_element(self.avatar_image)
        self.click_on_element(self.settings_tab)
        self.click_on_element(self.logout_button)

