from hgaconfig.Constants import *
from hgapages.android import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy




class LaunchScreenPage(HomeScreenPage.HomeScreenPage):

    # Elements at launch screen page
    play_as_guest_button = (MobileBy.ID, 'buttonPlayAsGuest')
    login_button_launch_screen = (MobileBy.ID, 'buttonLogin')
    launch_screen_text = (MobileBy.ID, 'tvAlreadyHaveAccount')

