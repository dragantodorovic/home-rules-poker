from hgaconfig import LoggingConfig as log
import time

from selenium.common.exceptions import NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from hgaconfig.Constants import *
from appium import webdriver


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver


#--------------------------------Methods---------------------------------------------------------------------
    # Puts app in background
    def put_app_in_background(self, seconds=None):
        if seconds is None:
            self.seconds = Constants.time_for_app_in_background
        else:
            self.seconds = seconds
        self.driver.background_app(self.seconds)

    # Terminates app and activate it again
    def terminate_and_activate_app(self, pause_interval=None):
        if pause_interval is None:
            self.pause_interval = 0
        else:
            self.pause_interval = pause_interval
            self.driver.close_app()
            time.sleep(self.pause_interval)
            self.driver.launch_app()
            time.sleep(2)

    # Find element
    def find_element(self, locator, max_wait=20, find_all_elements=False):

        if find_all_elements:
            find_function = expected_conditions.presence_of_all_elements_located
        else:
            find_function = expected_conditions.presence_of_element_located

        try:
            element = WebDriverWait(self.driver, max_wait).until(find_function(locator))
            return element
        except (TimeoutException, WebDriverException, NoSuchElementException):
            return None

    # Click on element
    def click_on_element(self, locator, max_wait=20):
        self.find_element(locator, max_wait).click()

    def hide_keyboard(self):
        try:
            self.driver.press_keycode(111)

        except:
            pass

    def button_is_shown(self, button_label, button_text):
        value = button_label.is_displayed()
        text = button_label.get_attribute('text')
        if (button_text == text) and value:
            log.logging.info('{} button is shown properly'.format(button_text))
            return True

        else:
            log.logging.info('{} button is not shown properly'.format(button_text))
            return False

    def label_is_shown(self, label, text):
        element_text = self.find_element(label).get_attribute('text')
        if element_text == text:
            # log.logging.info('{} label is shown properly'.format(text))
            return True
        else:
            # log.logging.info('{} label is not shown properly'.format(text))
            return False
