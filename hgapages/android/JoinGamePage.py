from appium.webdriver.common.touch_action import TouchAction
from hgapages.android import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
from hgaconfig.Constants import *
import time


class JoinGamePage(HomeScreenPage.HomeScreenPage):

    # Elements on Take a seat page
    code_table_input_field = (MobileBy.ID, 'textInputEdit')
    cancel_button = (MobileBy.ID, 'buttonBottomLeft')
    set_buy_in_button = (MobileBy.ID, 'buttonBottomRight')
    buy_in_input_field = (MobileBy.ID, 'editBuyInValue')
    wallet_value_text = (MobileBy.ID, 'textInWalletValue')
    minimum_value_text = (MobileBy.ID, 'textSeekMinLabel')
    maximum_value_text = (MobileBy.ID, 'textSeekMaxLabel')
    buy_in_slider = (MobileBy.ID, 'seekBarBuy')

    #_______________________ Methods______________________________

    # Join table with desired number; if empty: join table with 1 bot
    # buy_in_amount default is table minimum
    def join_table(self, table_number=None, buy_in_amount=None):
        self.join_table_and_bring_in(table_number=table_number, buy_in_amount=buy_in_amount)
        self.start_game_in_join_game_flow()

    # Join table with desired table number; if empty: join table with 1 bot
    # buy_in_amount default is table minimum
    def  join_table_and_bring_in(self, table_number=None, buy_in_amount=None):
        if table_number is None:
            self.table_number = Constants.table_number_1bot
        else:
            self.table_number = table_number
        self.click_on_element(self.join_a_game_button)
        self.click_on_element(self.code_table_input_field)
        self.find_element(self.code_table_input_field).clear()
        self.find_element(self.code_table_input_field).send_keys(self.table_number)
        # removing keyboard from screen
        # self.driver.hide_keyboard()
        self.hide_keyboard()
        self.click_on_element(self.set_buy_in_button)
        if buy_in_amount is None:
            time.sleep(1.5)
            temp = self.get_minimum_value_text()
            self.buy_in_amount = temp
        else:
            self.buy_in_amount = buy_in_amount
        self.click_on_element(self.buy_in_input_field)
        self.find_element(self.buy_in_input_field).clear()
        self.find_element(self.buy_in_input_field).send_keys(self.buy_in_amount)
        self.hide_keyboard()


    # Helper method: Press Start Game option at Bring-in Screen (Join game flow)
    def start_game_in_join_game_flow(self):
        self.click_on_element(self.join_game_button)

    # Join empty table
    def join_empty_table(self):
        log.logging.info('Joining empty table: {}'. format(Constants.table_number_empty))
        self.join_table(Constants.table_number_empty)

    # Returning text value of minimum_value_field
    def get_minimum_value_text(self):
        element = self.find_element(self.minimum_value_text)
        log.logging.info('Minimum value is: {}'.format(element.get_attribute('text')))
        return element.get_attribute('text')