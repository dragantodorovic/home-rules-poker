from hgapages.web import BasePage as bp
from selenium.webdriver.common.by import By
from hgaconfig import LoggingConfig as log
import time


class ObserverGamePage(bp.BasePage):
    # Elements on Observer game page
    empty_seats = (By.CLASS_NAME, 'empty-seat')
    in_game_seats = (By.CLASS_NAME, 'player-in-game-content')
    dealer_chip = (By.CLASS_NAME, 'hra-dealer-chip')
    card_first = (By.ID, 'hra-card-0')
    card_second = (By.ID, 'hra-card-1')
    card_third = (By.ID, 'hra-card-2')
    card_fourth = (By.ID, 'hra-card-3')
    card_fifth = (By.ID, 'hra-card-4')
    main_pot = (By.CLASS_NAME, 'main-pot')
    player_name_0 = (By.ID, 'hra-player-0-name')
    player_name_1 = (By.ID, 'hra-player-1-name')
    player_name_2 = (By.ID, 'hra-player-2-name')
    player_name_3 = (By.ID, 'hra-player-3-name')
    player_name_4 = (By.ID, 'hra-player-4-name')
    player_money_0 = (By.ID, 'hra-player-0-money')
    player_money_1 = (By.ID, 'hra-player-1-money')
    player_money_2 = (By.ID, 'hra-player-2-money')
    player_money_3 = (By.ID, 'hra-player-3-money')
    player_money_4 = (By.ID, 'hra-player-4-money')
    action_amount_p0 = (By.XPATH, '//*[@id="hra-player-0-action-money"]/span[2]')
    action_amount_p1 = (By.XPATH, '//*[@id="hra-player-1-action-money"]/span[2]')
    action_amount_p2 = (By.XPATH, '//*[@id="hra-player-2-action-money"]/span[2]')
    action_amount_p3 = (By.XPATH, '//*[@id="hra-player-3-action-money"]/span[2]')
    action_amount_p4 = (By.XPATH, '//*[@id="hra-player-4-action-money"]/span[2]')
    game_code_number = (By.XPATH, '/html/body/hrabrowserpokerapplicationroot/hragameroot/homerulesscreen/homerulestablecontainer/fieldset/label')


    # --------------------------------Methods----------------------------------

    # Returns number of empty seats on table.
    def get_empty_seats_number(self):
        all_empty_seats_number = self.find_element(self.empty_seats, find_all_elements=True)
        log.logging.info('Number of empty seats at table: {}'.format(len(all_empty_seats_number)))
        return len(all_empty_seats_number)

    # Returns number of empty seats on table.
    def get_in_game_seats_number(self):
        all_in_game_seats_number = self.find_element(self.in_game_seats, find_all_elements=True)
        log.logging.info('Number of in_game seats at table: {}'.format(len(all_in_game_seats_number)))
        return len(all_in_game_seats_number)

    # Returns number of empty seats on table.
    def get_game_code_number(self):
        text = self.get_text_from_element(self.find_element(self.game_code_number))
        log.logging.info('Game code for selected Observer table is: {}'.format(text))
        return text

    # Returns number of position for Dealer chip, starting from zero
    def get_dealer_chip_position(self):
        dealer_chip_string_id = self.find_element(self.dealer_chip).get_attribute("id")
        log.logging.info('Dealer chip position is: {}'.format(dealer_chip_string_id[-1]))
        return int(dealer_chip_string_id[-1])

    # Returns players name based on position
    def get_players_name(self, position=None):
        switcher = {
            0: self.player_name_0,
            1: self.player_name_1,
            2: self.player_name_2,
            3: self.player_name_3,
            4: self.player_name_4,
        }
        selected_players_name = switcher.get(position, "nothing")
        text = self.get_text_from_element(self.find_element(selected_players_name))
        log.logging.info('Selected player {} name is: {}'.format(position, text.strip()))
        return text.strip()

    # Returns players money based on position
    def get_players_money(self, position=None):
        switcher = {
            0: self.player_money_0,
            1: self.player_money_1,
            2: self.player_money_2,
            3: self.player_money_3,
            4: self.player_money_4,
        }
        selected_players_money = switcher.get(position, "nothing")
        text = self.get_text_from_element(self.find_element(selected_players_money))
        log.logging.info('Selected player {} money amount is: {}'.format(position, text))
        return float(text)

    # Returns players money based on position
    def get_action_money(self, position=None):
        switcher = {
            0: self.action_amount_p0,
            1: self.action_amount_p1,
            2: self.action_amount_p2,
            3: self.action_amount_p3,
            4: self.action_amount_p4,
        }
        selected_action_money = switcher.get(position, "nothing")
        text = self.get_text_from_element(self.find_element(selected_action_money))
        log.logging.info('Selected action {} amount is: {}'.format(position, text))
        return text

    # Returns SB and BB values in case of 2 players sitting on table
    # Precondition is that scripts opens empty table (or with 1 player only)
    # Reason: Dealer chip changes position after 2nd player sits on table so validation should be done before that
    def get_SB_BB_values(self):
        dealer = self.get_dealer_chip_position()
        non_dealer = 0 if dealer == 1 else 1
        SB_value = self.get_action_money(position=non_dealer)
        BB_value = self.get_action_money(position=dealer)
        return [SB_value, BB_value]

    # Method for test NAPPS_1411 Host a game existent basic flow
    def pull_data_for_host_a_game_existent_basic_flow(self):
        table_empty_seat_number = self.get_empty_seats_number()
        table_in_game_seat_number = self.get_in_game_seats_number()
        table_balance_info = self.get_players_money(0)
        return [table_empty_seat_number, table_in_game_seat_number, table_balance_info]