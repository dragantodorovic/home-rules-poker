from hgapages.web import BasePage as bp
from selenium.webdriver.common.by import By
from hgaconfig.Constants import *
from hgaconfig import LoggingConfig as log
import time


class WelcomePage(bp.BasePage):
    # Elements on Welcome page
    game_code_input_field = (By.ID, 'hra-code')
    open_table_button = (By.ID, 'hra-open-table')

    # --------------------------------Methods----------------------------------

    def open_table(self, table_number):
        self.driver.get(Constants.observer_table_url)
        self.click_on_element(self.game_code_input_field)
        self.find_element(self.game_code_input_field).send_keys(table_number)
        self.click_on_element(self.open_table_button)
        time.sleep(2)
        if self.driver.current_url == Constants.observer_table_url:
            try:
                self.click_on_element(self.open_table_button)
            except:
                pass
        log.logging.info('Opening web table number: {}'.format(table_number))
