from hgaconfig import LoggingConfig as log
import time

from selenium.common.exceptions import NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from hgaconfig.Constants import *
from selenium import webdriver


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver

    # --------------------------------Methods---------------------------------------------------------------------

    # Find element
    def find_element(self, locator, max_wait=20, find_all_elements=False):

        if find_all_elements:
            find_function = expected_conditions.presence_of_all_elements_located
        else:
            find_function = expected_conditions.presence_of_element_located

        try:
            element = WebDriverWait(self.driver, max_wait).until(find_function(locator))
            return element
        except (TimeoutException, WebDriverException, NoSuchElementException):
            return None

    # Click on element
    def click_on_element(self, locator, max_wait=20):
        self.find_element(locator, max_wait).click()

    # Getting text from html element
    def get_text_from_element(self,element):
        return element.get_attribute('innerHTML')
