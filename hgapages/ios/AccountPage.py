from hgaconfig.Constants import *
from hgapages.ios import LaunchScreenPage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log


class AccountPage(LaunchScreenPage.LaunchScreenPage):

    # Elements on first page
    ok_popup_button = (MobileBy.ID, 'OK')
    nickname_field = (MobileBy.ID, 'txtNickname')
    proceed_button = (MobileBy.ID, 'btnAction')
    already_have_account_link = (MobileBy.ID, 'lblTextBottom')
    sign_in_button = (MobileBy.ID, 'btnBottom')

    # Elements on sign in page
    username_field = (MobileBy.ID, 'txtUsername')
    password_field = (MobileBy.ID, 'txtPassword')
    login_button = (MobileBy.ID, 'btnLogin')


    #---------------------------- Methods-------------------------------------------------

    def login_user(self, name=None):
        try:
            self.click_on_element(self.ok_popup_button)
        except:
            pass
        if name is None:
            self.name = Constants.account_username
        else:
            self.name = name
        self.click_on_element(self.login_button_launch_screen)
        # self.sign_in_button().click()
        log.logging.info('Entering username: {}'.format(name))
        self.click_on_element(self.username_field)
        self.find_element(self.username_field).send_keys(self.name)
        self.click_on_element(self.login_button)


    def login_guest(self):
        try:
            self.click_on_element(self.ok_popup_button)
        except:
            pass
        self.click_on_element(self.play_as_guest_button)