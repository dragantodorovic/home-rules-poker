from appium.webdriver.common.touch_action import TouchAction
from hgapages.ios import HomeScreenPage as hsp
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
import time
import re


class GameplayPage(hsp.HomeScreenPage):

    # Elements on On Turn page
    raise_button = (MobileBy.ID, 'btnRaise')
    check_button = (MobileBy.ID, 'btnCheck')
    raise_2bb_button = (MobileBy.ID, 'btnX2')
    raise_3bb_button = (MobileBy.ID, 'BtnX3')
    raise_3dots_button = (MobileBy.ID, 'btn3dots')
    all_in_button = (MobileBy.ID, 'btnX4')
    fold_button = (MobileBy.ID, 'btnFold')
    bet_button = (MobileBy.ID, 'btnBet')
    raise_picker_wheel_button = (MobileBy.XPATH, "//XCUIElementTypeApplication[@name=\"RevealerSK\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypePicker/XCUIElementTypePickerWheel")
    done_button = (MobileBy.ID, 'Done')
    call_button = (MobileBy.ID, 'btnCall')
    showdown_info = (MobileBy.ID, 'Showdown')
    lets_see_info = (MobileBy.ID, "Let's see who won this hand!")
    balance_lable = (MobileBy.ID, "lblBalance")
    picker_in_gameplay = (MobileBy.XPATH, "//XCUIElementTypePicker[@name=\"inGamePickerView\"]/XCUIElementTypePickerWheel")
    win_screen_image = (MobileBy.ID, "imgYouWin")
    win_screen_button = (MobileBy.ID, "btnPrimaryAction")
    bb_label = (MobileBy.ID, "lblBigBlind")
    sb_label = (MobileBy.ID, "lblSmallBlind")

    # Elements on Not On Turn page

    tease_button = (MobileBy.ID, "btnTease")
    call_advance_button = (MobileBy.ID, "advanceBtncall")
    call_any_advance_button = (MobileBy.ID, "advanceBtnCallAny")
    fold_advance_button = (MobileBy.ID, "advanceBtnFold")
    check_advance_button = (MobileBy.ID, "advanceBtnCheck")
    check_fold_advance_button = (MobileBy.ID, "advanceBtnCheckFold")
    post_bb_button = (MobileBy.ID, "POST BB")
    dealer_info = (MobileBy.ID, "isDealer")
    folded_page_image = (MobileBy.ID, "imgFolded")
    gameplay_page_text = (MobileBy.ID, "lblText")
    folded_page_subtext = (MobileBy.ID, "lblSubtext")

    # --------------------------------Helper Methods--------------------------------------------------------

    # function that returns if player is dealer or not
    def is_dealer(self):
        element = self.find_element(self.dealer_info)
        return element.get_attribute('value')

    # Method returns amount shown on the desire button
    def get_amount_from_the_button(self, button_name):
        button_text = button_name.get_attribute('label')
        number = re.findall("\d+\.\d+", button_text)
        log.logging.info("Amount from the button is: {}".format(number[0]))
        return number[0]

    # Method returns BB value
    def get_bb_value(self):
        element = self.find_element(self.bb_label)
        log.logging.info('BB is: {}'.format(element.get_attribute("label")))
        return element.get_attribute('label')

    # Method returns SB value
    def get_sb_value(self):
        element = self.find_element(self.sb_label)
        log.logging.info('SB is: {}'.format(element.get_attribute("label")))
        return element.get_attribute('label')

    #Method returs True if button is displayed properly
    #If regex is not needed, set regex_disable=True
    def button_is_shown(self, button_name, button_text, regex_disable=False):
        value = button_name.is_displayed()
        if not regex_disable:
            text = re.findall("[a-zA-Z]+", button_name.get_attribute('label'))
            if (button_text == text[0]) and value:
                log.logging.info('{} is shown properly'.format(button_text))
                return True
            else:
                return False
        if regex_disable:
            text = button_name.get_attribute('label')
            if (button_text == text) and value:
                log.logging.info('{} is shown properly'.format(button_text))
                return True
            else:
                return False

    def get_folded_page_text(self):
        element = self.find_element(self.gameplay_page_text)
        log.logging.info('Folded page text is: {}'.format(element.get_attribute('value')))
        return element.get_attribute('value')

    def get_folded_page_subtext(self):
        element = self.find_element(self.folded_page_subtext)
        log.logging.info('Folded page subtext is: {}'.format(element.get_attribute('value')))
        return element.get_attribute('value')

    def get_win_screen_text(self):
        element = self.find_element(self.win_screen_button)
        log.logging.info('Win screen text is: {}'.format(element.get_attribute('label')))
        return element.get_attribute('label')

    #-----------------------------Methods to work with picker wheel------------------------------------------------------------------------------------------------------------------------

   # Getting the whole picker value
    def get_whole_picker_value(self):
        element = self.find_element(self.picker_in_gameplay)
        return element.get_attribute('value')


    # Getting the number from picker value
    # Method returns number
    def get_the_number_from_picker_value(self, value):
        number = re.findall("\d+\.\d+", value)
        return number[0]

    # Getting the default picker value
    # Method returns only the number in string
    def get_default_picker_value(self):
        value = self.get_whole_picker_value()
        return self.get_the_number_from_picker_value(value)

    # Scroll the picker wheel by element
    def select_next_picker_wheel_value(self):
        self.driver.execute_script("mobile:selectPickerWheelValue",
                                   {'order': 'previous', 'offset': 0.15, 'element': self.find_element(self.picker_in_gameplay)})

    def select_previous_picker_wheel_value(self):
        self.driver.execute_script("mobile:selectPickerWheelValue",
                                   {'order': 'next', 'offset': 0.15, 'element': self.picker_in_gameplay})

    # Getting the picker list
    # Method returns list of amount from picker wheel - : ['3.0', '20.0', '40.0', '60.0', '70.0']
    # Parameter list - whole picker list
    def get_picker_list(self, list):
        new_list = []
        for x in list:
            new_list.append(self.get_the_number_from_picker_value(x))
        return new_list

    # Getting the list of multiplied bbs
    def get_list_with_multiplied_bbs(self, list):
        new_list = list[1:-1]
        list = [float(x) for x in new_list ]
        return list

    # Getting the all_in picker value from the whole picker list
    # Method has parameter list - whole picker list
    def get_all_in_picker_value(self, list):
        value = list[-1]
        return self.get_the_number_from_picker_value(value)

    # Getting  the whole pickeerList values
    # Method returns list -  ['3.0, (MIN)', '20.0, (10 BB)', '40.0, (20 BB)', '60.0, (30 BB)', '68.0, (ALL IN)']
    def get_whole_picker_list(self):
        picker_list_elements = []
        current_value = self.get_whole_picker_value()
        picker_list_elements.append(current_value)
        while (not ("ALL IN" in current_value)):
            self.select_next_picker_wheel_value()
            picker_list_elements.append(self.get_whole_picker_value())
            current_value = self.get_whole_picker_value()
        return picker_list_elements

    # Selecting the ALL IN value from picker wheel and confirming the selection
    # All in value will be shown on the button (RAISE ALL IN)
    def scroll_picker_list_to_all_in(self):
        current_value = self.get_whole_picker_value()
        while (not ("ALL IN" in current_value)):
            self.select_next_picker_wheel_value()
            current_value = self.get_whole_picker_value()
        self.click_on_element(self.done_button)

    # Getting BB, MIN and ALL IN  strings from picker wheel
    # Method returns list of elements: ['MIN', '10 BB', '20 BB', '30 BB', 'ALL IN']
    # Argument list - all picker wheel list
    def get_elements_in_brackets(self, list):
        new_list = []
        for x in list:
            new_list.append(re.findall("\d+\.\d+, \(([\w\s]+)\)", x)[0])
        return new_list


    # Method that returns TRUE if 10 BB, 20 BB, 30 BB are shown properly > (30 BB)

    def picker_list_BB_shown_properly(self, list):
        if all(x in list for x in ['10 BB', '20 BB', '30 BB']):
            return True


    #Method returns list based on BB
    # i.e . bb -2  => list is: [20.0, 40.0, 60.0]
    #Note - list of float values
    def create_list_based_on_BB(self,BB):
        list = [10, 20, 30]
        desired_list = []
        for x in list:
            value = x * float(BB)
            desired_list.append(value)
        return  desired_list


    # Method compares to list
    #list_from_app - self.get_list_with_multiplied_bbs
    # list_based_on_BB - self.create_list_based_on_BB
    def picker_list_values_shown_properly(self, list_from_app, list_based_on_BB):
        if list_from_app == list_based_on_BB:
            return True

        else:
            return False


    #---------------------------------------Methods Actions-----------------------------------------------------


    def check_who_is_dealer(self, gameplay_page, gameplay_page2):
        if gameplay_page.is_dealer() == "true":
            dealer = gameplay_page
            non_dealer = gameplay_page2
        else:
            dealer = gameplay_page2
            non_dealer = gameplay_page
        return  [dealer, non_dealer]

    def check_who_is_dealer_for_settings(self, gameplay_page, settings_page, setting_page2):
        if gameplay_page.is_dealer() == "true":
            dealer = settings_page
            non_dealer = setting_page2
        else:
            dealer = setting_page2
            non_dealer = settings_page
        return [dealer, non_dealer]

    def play_for_all_in_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.call_any_advance_button)
        dealer.click_on_element(self.raise_3dots_button)
        dealer.scroll_picker_list_to_all_in()
        dealer.click_on_element(self.raise_button)

    def play_for_as_call_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_any_advance_button)
        AMOUNT_OF_RAISE = non_dealer.get_amount_from_the_button(self.find_element(self.raise_button))
        non_dealer.click_on_element(self.raise_button)
        time.sleep(5)
        BALANCE_INFO_SECOND = dealer.get_current_amount()
        dealer.click_on_element(self.call_any_advance_button)
        non_dealer.click_on_element(self.check_button)
        time.sleep(3)
        non_dealer.click_on_element(self.bet_button)
        dealer.click_on_element(self.call_button)
        non_dealer.click_on_element(self.check_button)
        dealer.click_on_element(self.check_button)
        return  [AMOUNT_OF_RAISE, BALANCE_INFO_SECOND]

    def play_for_as_check_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_any_advance_button)
        non_dealer.click_on_element(self.raise_button)
        time.sleep(5)
        BALANCE_INFO_FIRST = dealer.get_current_amount()
        dealer.click_on_element(self.call_any_advance_button)
        non_dealer.click_on_element(self.check_button)
        time.sleep(3)
        non_dealer.click_on_element(self.bet_button)
        BALANCE_INFO_SECOND = dealer.get_current_amount()
        dealer.click_on_element(self.call_button)
        non_dealer.click_on_element(self.check_button)
        dealer.click_on_element(self.check_button)
        return [BALANCE_INFO_FIRST, BALANCE_INFO_SECOND ]

    def play_for_check_fold_as_check_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.check_fold_advance_button)
        non_dealer.click_on_element(self.call_button)
        if non_dealer.button_is_shown(self.find_element(self.bet_button), "BET"):
            BALANCE_FIRST = dealer.get_current_amount()
        non_dealer.click_on_element(self.check_button)
        if non_dealer.button_is_shown(self.find_element(self.bet_button), "BET"):
            BALANCE_SECOND = dealer.get_current_amount()
        return [BALANCE_FIRST,BALANCE_SECOND ]

    def play_for_check_fold_as_fold_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.check_fold_advance_button)
        PLAYER_BALANCE_BEFORE_FOLD = self.get_current_amount()
        non_dealer.click_on_element(self.raise_button)
        time.sleep(5.5)
        if dealer.button_is_shown(dealer.find_element(dealer.check_advance_button), 'CHECK'):
            PLAYER_BALANCE_AFTER_FOLD = self.get_current_amount()
        non_dealer.click_on_element(self.plus_button)
        return [PLAYER_BALANCE_BEFORE_FOLD,PLAYER_BALANCE_AFTER_FOLD ]

    def play_for_NAPPS_678_test(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.call_button)
        time.sleep(4.5)
        # Getting dealers balance before it played advanced check button
        BALANCE_BEFORE = dealer.get_current_amount()
        dealer.click_on_element(self.check_advance_button)
        non_dealer.click_on_element(self.check_button)
        time.sleep(3)
        non_dealer.click_on_element(self.bet_button)
        non_dealer.click_on_element(self.check_advance_button)
        # Getting dealers balance after it played advanced check button
        BALANCE_AFTER = dealer.get_current_amount()
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.call_button)
        non_dealer.click_on_element(self.check_button)
        dealer.click_on_element(self.check_button)
        return [BALANCE_BEFORE,BALANCE_AFTER ]

    def play_for_win_screen_test(self, dealer, non_dealer):
        dealer.click_on_element(self.fold_button)
        non_dealer.click_on_element(self.win_screen_button)
        win_screen_text = non_dealer.get_win_screen_text()
        return win_screen_text

    def before_bring_more_chips_gameplay(self, dealer, non_dealer):
        amount_before_bringing_more_chips = non_dealer.get_current_amount()
        BB = non_dealer.get_bb_value()
        sum = float(BB) + float(amount_before_bringing_more_chips)
        log.logging.info('Amount after bringing more chips: {}'.format(sum))
        return float(sum)

    def bring_more_chips_gameplay(self, dealer, non_dealer):
        dealer.click_on_element(self.fold_button)
        time.sleep(6)
        amount_after_bringing_more_chips = non_dealer.get_current_amount()
        log.logging.info('Amount after bringing more chips: {}'.format(amount_after_bringing_more_chips))
        return float(amount_after_bringing_more_chips)

    def play_for_hight_bet_mechanism_test(self):
        self.click_on_element(self.raise_3dots_button)
        current_raise_button_amount = self.get_amount_from_the_button(self.find_element(self.raise_button))
        log.logging.info('Amount: {}'.format(current_raise_button_amount))
        current_amount = self.get_current_amount()
        log.logging.info('Total amount: {}'.format(current_amount))
        lowest_picker_value = self.get_default_picker_value()
        log.logging.info('Lowest picker value: {}'.format(lowest_picker_value))
        log.logging.info('Getting all in picker value ')
        whole_list = self.get_whole_picker_list()
        elements_in_brackets = self.get_elements_in_brackets(whole_list)
        all_in_picker_value = self.get_all_in_picker_value(whole_list)
        log.logging.info('All in picker value: {}'.format(all_in_picker_value))
        bb_value = self.get_bb_value()
        list_bb = self.create_list_based_on_BB(BB=bb_value)
        list_with_multiplied_bbs = self.get_list_with_multiplied_bbs(self.get_picker_list(whole_list))
        self.click_on_element(self.raise_3dots_button)
        return [current_amount, all_in_picker_value, current_raise_button_amount,lowest_picker_value, elements_in_brackets, list_bb, list_with_multiplied_bbs]


    def accelerator_buttons_are_shown_properly(self):
        bb3_is_shown = self.button_is_shown(self.find_element(self.raise_2bb_button),'2BB',regex_disable=True)
        bb4_is_shown = self.button_is_shown(self.find_element(self.raise_3bb_button),'3BB',regex_disable=True)
        dots_are_shown = self.button_is_shown(self.find_element(self.raise_3dots_button),'dotsSlice',regex_disable=True)
        all_in_button_is_shown = self.button_is_shown(self.find_element(self.all_in_button),'ALL IN',regex_disable=True)
        value = bb3_is_shown and bb4_is_shown and dots_are_shown and all_in_button_is_shown
        if value:
            log.logging.info('Accelerator buttons are shown properly')
            return True
        else:
            log.logging.info('Accelerator buttons are not shown properly')
            return False

    def play_for_NAPPS_1827(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        time.sleep(2)
        raise_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_button)))
        log.logging.info('Raise button value before: {}'.format(raise_button_value_before))
        non_dealer.click_on_element(non_dealer.raise_2bb_button)
        raise_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.raise_button))
        log.logging.info('Raise button value after: {}'.format(raise_button_value_after))
        return [raise_button_value_before,raise_button_value_after]

    def play_for_NAPPS_1833(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        time.sleep(2)
        raise_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.raise_button)))
        log.logging.info('Raise button value before: {}'.format(raise_button_value_before))
        non_dealer.click_on_element(non_dealer.all_in_button)
        raise_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.raise_button))
        log.logging.info('Raise button value after: {}'.format(raise_button_value_after))
        return [raise_button_value_before,raise_button_value_after]

    def play_for_NAPPS_1837(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_button)
        time.sleep(2)
        bet_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.bet_button)))
        log.logging.info('Bet button value before: {}'.format(bet_button_value_before))
        non_dealer.click_on_element(non_dealer.raise_2bb_button)
        bet_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.bet_button))
        log.logging.info('Bet button value after: {}'.format(bet_button_value_after))
        return [bet_button_value_before,bet_button_value_after]

    def play_for_NAPPS_1838(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_button)
        time.sleep(2)
        bet_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.bet_button)))
        log.logging.info('Bet button value before: {}'.format(bet_button_value_before))
        non_dealer.click_on_element(non_dealer.raise_3bb_button)
        bet_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.bet_button))
        log.logging.info('Bet button value after: {}'.format(bet_button_value_after))
        return [bet_button_value_before,bet_button_value_after]

    def play_for_NAPPS_1840(self, dealer, non_dealer):
        dealer.click_on_element(self.raise_button)
        non_dealer.click_on_element(self.raise_button)
        dealer.click_on_element(self.call_button)
        time.sleep(2)
        bet_button_value_before = float(
            non_dealer.get_amount_from_the_button(non_dealer.find_element(non_dealer.bet_button)))
        log.logging.info('Bet button value before: {}'.format(bet_button_value_before))
        non_dealer.click_on_element(non_dealer.all_in_button)
        bet_button_value_after = non_dealer.get_amount_from_the_button(
            non_dealer.find_element(non_dealer.bet_button))
        log.logging.info('Bet button value after: {}'.format(bet_button_value_after))
        return [bet_button_value_before,bet_button_value_after]