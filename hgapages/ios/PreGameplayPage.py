from appium.webdriver.common.touch_action import TouchAction
from hgapages.ios import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
from hgaconfig.Constants import *



class HostGamePage(HomeScreenPage.HomeScreenPage):

    # Elements on Pre-Gameplay page

    post_bb_button = (MobileBy.ID, 'POST BB')

