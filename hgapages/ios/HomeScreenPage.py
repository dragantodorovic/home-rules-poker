from hgapages.ios import BasePage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log


class HomeScreenPage(BasePage.BasePage):


    # Elements on Welcome back page
    join_a_game_button = (MobileBy.ID, 'btnJoinGame')
    host_a_game_button = (MobileBy.ID, 'btnHostGame')
    shop_button = (MobileBy.ID, 'btnShop')
    settings_button = (MobileBy.ID, 'btnSettings')
    avatar_image = (MobileBy.ID, 'imgAvatar')
    avatar_button = (MobileBy.ID, 'btnAvatar')
    nickname_info = (MobileBy.ID, 'lblNickname')
    balance_info = (MobileBy.ID, 'lblBalance')
    settings_tab = (MobileBy.ID, 'Settings')

    #Button name is Take a Seat but it is already used and it is in Sit back dialog
    sit_back_button = (MobileBy.ID, 'btnSitBack')

    # Buy-in page
    minus_button = (MobileBy.ID, 'btnMinus')
    plus_button = (MobileBy.ID, 'btnPlus')
    bring_in_amount_info = (MobileBy.ID, 'lblBringInAmount')
    amount_slider = (MobileBy.ID, 'slider')
    cancel_button = (MobileBy.ID, 'btnCancel')
    start_game_button = (MobileBy.ID, 'btnStart')

# ---------------------------Methods-------------------------------------

    # Shows amount for bring in field
    def get_bring_in_amount(self):
        element = self.find_element(self.bring_in_amount_info)
        return element.get_attribute("value")


    # returns player's balance which is next to Avatar
    def get_current_amount(self):
        element = self.find_element(self.balance_info)
        log.logging.info('Current balance is:{}'.format(element.get_attribute("value")))
        return element.get_attribute("value")

