from hgapages.ios import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
from hgaconfig import Constants as con
import time


class SettingsPage(HomeScreenPage.HomeScreenPage):
    # Elements on Settings page - not in game
    sounds_title = (MobileBy.ID, 'titleSounds')
    sounds_description = (MobileBy.ID, 'subtitleSounds')
    sounds_toggle_button = (MobileBy.ID, 'switchSounds')
    about_title = (MobileBy.ID, 'titleAbout')
    about_description = (MobileBy.ID, 'subtitleAbout')
    logOut_title = (MobileBy.ID, 'titleLogout')
    logOut_description = (MobileBy.ID, 'subtitleLogout')

    # Elements on About Page
    about_redLark = (MobileBy.ID, 'titleAboutredlark')
    terms_and_conditions = (MobileBy.ID, 'titleTermsconditions')
    privacy_policy = (MobileBy.ID, 'titlePrivacypolicy')
    version_info = (MobileBy.ID, 'titleAppversion')

    # Elements on Game option (Settings page) - in game
    # close_X_button = (MobileBy.ID, 'btnX')
    close_X_button = (MobileBy.XPATH, '//XCUIElementTypeButton[@name="btnX"]')

    table_info_button = (MobileBy.ID, 'titleTableinfo')
    table_info_description = (MobileBy.ID, 'subtitleTableinfo')
    bring_more_chips_button = (MobileBy.ID, 'titleBringmorechips')
    bring_more_chips_description = (MobileBy.ID, 'subtitleBringmorechips')
    leaderboard_button = (MobileBy.ID, 'titleLeaderboard')
    leaderboard_description = (MobileBy.ID, 'subtitleLeaderboard')
    sit_out_switch = (MobileBy.ID, 'switchSitOut')
    sit_out_button = (MobileBy.ID, 'titleSitOut')
    sit_out_description = (MobileBy.ID, 'subtitleSitOut')
    # pause_game_button = (MobileBy.ID, 'titlePausegame')
    # end_game_button = (MobileBy.ID, 'titleEndgame')
    leave_game_button = (MobileBy.ID, 'titleLeavegame')
    leave_game_description = (MobileBy.ID, 'subtitleLeavegame')
    leave_table_popup_button = (MobileBy.ID, 'btnOk')
    cancel_button = (MobileBy.ID, 'btnCancel')
    sit_out_popup_button = (MobileBy.ID, 'seatOutPopUp')
    leave_game_pop_up_description = (MobileBy.ID, 'lblPopup')
    leave_game_pop_up_cancel = (MobileBy.ID, 'btnCancel')

    # Leaderboard page
    back_arrow_button = (MobileBy.ID, 'btnBack')
    leaderboard_string = (MobileBy.ID, 'Leaderboard')
    ranking_number = (MobileBy.ID, '1')
    player_name = (MobileBy.ID, 'autop1')
    bring_in_string = (MobileBy.ID, 'Bring-in:')
    win_loss_string = (MobileBy.ID, 'Win/Loss:')
    player_amount = (MobileBy.ID, '40.0')
    sitting_out_info = (MobileBy.ID, 'Sitting out')
    i_m_back_button = (MobileBy.ID, "I'M BACK")

    # Bring more chips page
    bring_in_title = (MobileBy.NAME, 'Bring-in')
    bring_in_input_field = (MobileBy.NAME, 'txtBringIn')
    bring_in_slider = (MobileBy.NAME, 'slider')
    bring_in_minimum_amount = (MobileBy.NAME, 'lblBringinAmount1')
    bring_in_maximum_amount = (MobileBy.NAME, 'lblBringinAmount5')
    bring_chips_button = (MobileBy.NAME, 'btnStart')

    # pop-up (bring more chips)
    pop_up_text_bring_in = (MobileBy.ID, 'lblPopup')
    yes_button_bring_in = (MobileBy.NAME, 'btnOk')
    cancel_button_for_bring_in = (MobileBy.NAME, 'btnCancel')

    # ---------------------------------- Methods---------------------------------------

    def click_on_cancel_button_for_bring_in(self):
        the_cancel_button_for_bring_in = self.find_element(self.cancel_button_for_bring_in, find_all_elements=True)
        the_cancel_button_for_bring_in[1].click()

    def sit_out_player(self):
        log.logging.info('Siting out player')
        self.click_on_element(self.settings_tab)
        self.click_on_element(self.sit_out_switch)
        self.click_on_element(self.close_X_button)

    def leave_game_player(self):
        log.logging.info('Leaving the game')
        self.click_on_element(self.settings_button)
        self.click_on_element(self.leave_game_button)
        self.click_on_element(self.leave_table_popup_button)

    def settings_page_is_shown_properly(self):
        sounds_is_shown_properly = self.label_is_shown(self.sounds_title, "Sounds")
        sounds_description_is_shown_properly = self.label_is_shown(self.sounds_description, 'Sound is ON')
        about_is_shown_proprly = self.label_is_shown(self.about_title, "About")
        about_description_is_shown_properly = self.label_is_shown(self.about_description,
                                                                  'More about the app, terms of service and privacy policy')
        log_out_is_shown_properly = self.label_is_shown(self.logOut_title, 'Log out')
        log_out_description_is_shown_properly = self.label_is_shown(self.logOut_description,
                                                                    '...and make sure to come back soon!')
        value = self.find_element(self.sounds_toggle_button).is_displayed()
        if sounds_is_shown_properly and sounds_description_is_shown_properly and about_is_shown_proprly and about_description_is_shown_properly and log_out_is_shown_properly and log_out_description_is_shown_properly and value:
            log.logging.info('Settings page is shown properly')
            return True
        else:
            log.logging.info('Settings page is not shown properly')
            return False

    def about_page_is_shown_properly(self):
        about_redLark_is_shown_propelry = self.label_is_shown(self.about_redLark, 'About RedLark')
        terms_and_conditions_is_shown_properly = self.label_is_shown(self.terms_and_conditions, 'Terms & conditions')
        privacy_policy_is_shown_properly = self.label_is_shown(self.privacy_policy, 'Privacy policy')
        # build_version_is_shown_propelry =  self.label_is_shown(self.version_info, 'Version: {}'.format(build_number))
        if about_redLark_is_shown_propelry and terms_and_conditions_is_shown_properly and privacy_policy_is_shown_properly:
            log.logging.info('About page is shown properly')
            return True
        else:
            log.logging.info('About page is not shown properly')
            return False

    def game_options_page_is_shown_properly(self):
        table_info_shown_properly = self.label_is_shown(self.table_info_button, "Table Info")
        table_info_description_is_shown_properly = self.label_is_shown(self.table_info_description,
                                                                       'Info about current table')
        bring_more_chips_shown_properly = self.label_is_shown(self.bring_more_chips_button, "Bring more chips")
        bring_more_chips_description_is_shown_properly = self.label_is_shown(self.bring_more_chips_description,
                                                                             'Get some fresh chips in the game')
        leaderboard_shown_properly = self.label_is_shown(self.leaderboard_button, 'Leaderboard')
        leaderboard_description_is_shown_properly = self.label_is_shown(self.leaderboard_description,
                                                                        'See how you stand against yor friends')
        sitout_shown_properly = self.label_is_shown(self.sit_out_button, "Sit out")
        sitout_description_shown_properly = self.label_is_shown(self.sit_out_description,
                                                                "Take a break for a few hands")
        leave_game_shown_properly = self.label_is_shown(self.leave_game_button, "Leave game")
        leave_game_description_shown_properly = self.label_is_shown(self.leave_game_description,
                                                                    "Stand out and leave the current game")
        value = self.find_element(self.sit_out_switch).is_displayed()
        if table_info_shown_properly and table_info_description_is_shown_properly and bring_more_chips_shown_properly and bring_more_chips_description_is_shown_properly and \
                leaderboard_shown_properly and leaderboard_description_is_shown_properly and sitout_shown_properly and sitout_description_shown_properly and \
                leave_game_shown_properly and leave_game_description_shown_properly and value:
            log.logging.info('Game options page is shown properly')
            return True
        else:
            log.logging.info('Game options page is not shown properly')
            return False

    def leave_game_page_is_shown_properly(self):
        desciption_shown_properly = self.label_is_shown(self.leave_game_pop_up_description,
                                                        'Are you leaving the game for real?')
        cancel_buton_is_shown_properly = self.button_is_shown(self.find_element(self.leave_game_pop_up_cancel),
                                                              'Cancel')
        leave_buton_is_shown_properly = self.button_is_shown(self.find_element(self.leave_table_popup_button),
                                                             'Leave Game')
        value = desciption_shown_properly and cancel_buton_is_shown_properly and leave_buton_is_shown_properly
        if value:
            log.logging.info('Leave game page is shown properly')
            return True
        else:
            log.logging.info('Leave game page is not shown properly')
            return False

    def leaderboard_page_is_shown_properly(self):
        leaderboard_string_is_shown = self.label_is_shown(self.leaderboard_string, 'Leaderboard')
        x_button_is_shown = self.find_element(self.close_X_button).is_displayed()
        player_rank_is_shown = self.label_is_shown(self.ranking_number, '1')
        bring_in_string_is_shown = self.label_is_shown(self.bring_in_string, 'Bring-in:')
        value = con.Constants.account_username

        win_loss__string_is_shown = self.label_is_shown(self.win_loss_string, 'Win/Loss:')
        player_name_is_shown_propelry = self.label_is_shown(self.player_name, value)

        value = leaderboard_string_is_shown and x_button_is_shown and player_rank_is_shown and bring_in_string_is_shown and win_loss__string_is_shown and player_name_is_shown_propelry
        if value:
            log.logging.info('Leaderboard page is shown properly')
            return True
        else:
            log.logging.info('Leaderboard page is not shown properly')
            return False

    def bring_more_chips(self, chips_amount=None):
        self.click_on_element(self.settings_button)
        self.click_on_element(self.bring_more_chips_button)
        self.click_on_element(self.bring_in_input_field)
        self.find_element(self.bring_in_input_field).clear()
        if chips_amount is None:
            minimum = self.find_element(self.bring_in_minimum_amount).get_attribute('value')
            self.chips_amount = minimum
        else:
            self.chips_amount = chips_amount
        self.find_element(self.bring_in_input_field).send_keys(self.chips_amount)
        # removing keyboard from screen
        self.driver.execute_script("mobile: swipe", {"direction": "left"})
        self.click_on_element(self.bring_chips_button)
        time.sleep(1.5)
        self.click_on_element(self.yes_button_bring_in)
        log.logging.info('Bring more chips added, amount:{}'.format(self.chips_amount))
