from appium.webdriver.common.touch_action import TouchAction
from hgapages.ios import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
from hgaconfig.Constants import *
from hgaconfig import LoggingConfig as log


class JoinGamePage(HomeScreenPage.HomeScreenPage):

    # Elements on Take a seat page
    code_table_input_field = (MobileBy.ID, 'txtCode')
    back_button = (MobileBy.ID, 'btnBack')
    take_a_seat_button = (MobileBy.ID, 'btnTakeSeat')
    buy_in_input_field = (MobileBy.ID, 'txtBringIn')
    # this element value returns `Balance: 10022.61` - not just numberic value
    wallet_value_text = (MobileBy.ID, 'lblInWallet')
    minimum_value_text = (MobileBy.ID, 'lblBringinAmount1')
    maximum_value_text = (MobileBy.ID, 'lblBringinAmount5')
    buy_in_slider = (MobileBy.ID, 'slider')

    #-----------------------Methods---------------------------------------------------

    # Join table with desired number; if empty: join table with 1 bot
    # buy_in_amount default is table minimum
    def join_table(self, table_number=None, buy_in_amount=None):
        self.join_table_and_bring_in(table_number=table_number, buy_in_amount=buy_in_amount)
        self.start_game_in_join_game_flow()

    # Helper method: Join_table flow reaches Bring-in screen
    def join_table_and_bring_in(self, table_number=None, buy_in_amount=None):
        if table_number is None:
            self.table_number = Constants.table_number_1bot
        else:
            self.table_number = table_number
        self.click_on_element(self.join_a_game_button)
        self.click_on_element(self.code_table_input_field)
        self.find_element(self.code_table_input_field).clear()
        self.find_element(self.code_table_input_field).send_keys(self.table_number)
        # removing keyboard from screen
        self.driver.execute_script("mobile: swipe", {"direction": "left"})
        self.click_on_element(self.take_a_seat_button)
        if buy_in_amount is None:
            temp = self.get_minimum_value_text()
            self.buy_in_amount = temp
        else:
            self.buy_in_amount = buy_in_amount
        self.click_on_element(self.buy_in_input_field)
        self.find_element(self.buy_in_input_field).clear()
        self.find_element(self.buy_in_input_field).send_keys(self.buy_in_amount)
        # removing keyboard from screen
        self.driver.execute_script("mobile: swipe", {"direction": "left"})

    # Helper method: Press Start Game option at Bring-in Screen (Join game flow)
    def start_game_in_join_game_flow(self):
        self.click_on_element(self.start_game_button)

    # Join empty table - uses join table method with predefined empty table
    def join_empty_table(self):
        log.logging.info('Joining empty table: {}'. format(Constants.table_number_empty))
        self.join_table(Constants.table_number_empty)

    # Returning text value of minimum_value_field
    def get_minimum_value_text(self):
        element = self.find_element(self.minimum_value_text)
        log.logging.info('Minimum value is: {}'.format(element.get_attribute('value')))
        return element.get_attribute('value')