from appium.webdriver.common.touch_action import TouchAction
from hgapages.ios import HomeScreenPage as hsp
from appium.webdriver.common.mobileby import MobileBy
from hgaconfig import LoggingConfig as log
import time
import re


class HostGamePage(hsp.HomeScreenPage):

    # Elements on host a game page
    back_button = (MobileBy.ID, 'btnBack')
    create_table_button= (MobileBy.ID, 'btnCreateTable')
    pick_your_bring_in = (MobileBy.ID, 'Pick your bring-in:')
    bring_in_25BB = (MobileBy.ID, '25 BB')
    bring_in_50BB = (MobileBy.ID, '50 BB')
    bring_in_75BB = (MobileBy.ID, '75 BB')
    bring_in_100BB = (MobileBy.ID, '100 BB')
    pick_your_blinds_text = (MobileBy.ID, 'Pick your blinds:')
    blinds_1_2 = (MobileBy.ID, '1/2')
    blinds_2_4 = (MobileBy.ID, '2/4')
    blinds_5_10 = (MobileBy.ID, '5/10')
    blinds_10_20 = (MobileBy.ID, '10/20')
    blinds_25_50 = (MobileBy.ID, '25/50')
    choose_number_of_seats_text = (MobileBy.ID, 'Choose number of seats:')
    number_of_seats_2 = (MobileBy.ID, 'btn2')
    number_of_seats_3 = (MobileBy.ID, 'btn3')
    number_of_seats_4 = (MobileBy.ID, 'btn4')
    number_of_seats_5 = (MobileBy.ID, 'btn5')
    number_of_seats_6 = (MobileBy.ID, 'btn6')
    number_of_seats_7 = (MobileBy.ID, 'btn7')
    number_of_seats_8 = (MobileBy.ID, 'btn8')
    number_of_seats_9 = (MobileBy.ID, 'btn9')

    # Elements on create table page
    cancel_button = (MobileBy.ID, 'btnCancel')
    try_again_button = (MobileBy.ID, 'btnOk')
    unable_to_host_such_table_text = (MobileBy.ID, 'lblPopup')
    table_device_link = (MobileBy.ID, 'btnTableDeviceLink')
    table_device_link_text = (MobileBy.ID, 'Table device link:')
    player_device_code_text= (MobileBy.ID, 'Player device code:')
    you_are_all_set_text = (MobileBy.ID, 'You are all set!')
    player_device_code = (MobileBy.ID, 'lblPlayerDeviceCode')
    ok_button = (MobileBy.ID, 'btnTakeSeat')


    #---------------------------------Methods-----------------------------

    def set_up_nonexistent_env(self):
        log.logging.info('Setting up environment for nonexistent flow')
        self.set_up_table(9, 50, 100)

    def set_up_existent_env(self):
        log.logging.info('Setting up environment for existent flow')
        [number_of_seats, values_of_blinds, values_of_bring_in] = self.set_up_table(3, 2, 25)
        return [number_of_seats, values_of_blinds, values_of_bring_in]

    def non_existent_dialog_is_shown_properly(self):
        log.logging.info('Verifying pop up dialog is shown properly:')
        pop_up_dialog_text = self.find_element(self.unuble_to_host_such_table_text).get_attribute('label')
        cancel_button = self.find_element(self.cancel_button)
        try_again = self.find_element(self.try_again_button)
        cancel_button_is_shown = self.button_is_shown(cancel_button, 'Cancel')
        try_again_button_is_shown = self.button_is_shown(try_again, 'Try again')
        if pop_up_dialog_text == 'Unable to host such table at this moment':
            value = True
            log.logging.info('Pop up dialog text is shown properly')
        else:
            value = False
        if cancel_button_is_shown and try_again_button_is_shown and value:
            return True
        else:
            False

    def table_info_screen_is_shown_properly(self):
        log.logging.info('Verifying that table info screen is shown properly:')
        you_are_all_set_text = self.find_element(self.you_are_all_set_text).get_attribute('value')
        table_device_link_text = self.find_element(self.table_device_link_text).get_attribute('value')
        player_device_code_text = self.find_element(self.player_device_code_text).get_attribute('value')
        ok_button = self.find_element(self.ok_button)
        ok_button_is_shown = self.button_is_shown(ok_button, 'OK')
        if you_are_all_set_text == "You are all set!" and table_device_link_text == "Table device link:" and player_device_code_text == "Player device code:":
            value = True
            log.logging.info('Table info screen text is shown properly')
        else:
            value = False
        if ok_button_is_shown and value:
            return True
        else:
            False

    def host_a_game_nonexistent_flow_cancel_scenario(self):
        self.click_on_element(self.host_a_game_button)
        self.set_up_nonexistent_env()
        log.logging.info('Clicking on create table button')
        log.logging.info('Clicking on cancel button')
        self.click_on_element(self.cancel_button)
        element = self.find_element(self.pick_your_blinds_text)
        pick_your_blinds_text = element.get_attribute('label')
        return pick_your_blinds_text


    def host_a_game_nonexistent_flow_try_again_scenario(self):
        self.click_on_element(self.host_a_game_button)
        self.set_up_nonexistent_env()
        log.logging.info('Clicking on create table button')
        self.non_existent_dialog_is_shown_properly()
        log.logging.info('Clicking on try again button')
        self.click_on_element(self.try_again_button)
        value = self.non_existent_dialog_is_shown_properly()
        return value

    def host_a_game_existent_flow_table_info_screen(self):
        self.click_on_element(self.host_a_game_button)
        self.set_up_existent_env()
        log.logging.info('Clicking on create table button')
        value = self.table_info_screen_is_shown_properly()
        return value

    def host_a_game_existent_flow_web_validation(self):
        self.click_on_element(self.host_a_game_button)
        [number_of_seats, values_of_blinds, values_of_bring_in] = self.set_up_existent_env()
        log.logging.info('Clicking on create table button')
        device_code = self.find_element(self.player_device_code).get_attribute('value')
        self.click_on_element(self.ok_button)
        return [number_of_seats, values_of_blinds, values_of_bring_in, device_code]

    def set_up_table(self, number_of_seats=None, values_of_blinds=None, values_of_bring_in=None):
        switcher_number_of_seats = {
            2: self.number_of_seats_2,
            3: self.number_of_seats_3,
            4: self.number_of_seats_4,
            5: self.number_of_seats_5,
            6: self.number_of_seats_6,
            7: self.number_of_seats_7,
            8: self.number_of_seats_8,
            9: self.number_of_seats_9
        }
        chosen_seats = switcher_number_of_seats.get(number_of_seats, self.number_of_seats_2)

        switcher_value_of_blinds = {
            2: self.blinds_1_2,
            4: self.blinds_2_4,
            10: self.blinds_5_10,
            20: self.blinds_10_20,
            50: self.blinds_25_50
        }
        chosen_value_of_blinds = switcher_value_of_blinds.get(values_of_blinds, self.blinds_1_2)

        switcher_values_of_bring_in = {
            25: self.bring_in_25BB,
            50: self.bring_in_50BB,
            75: self.bring_in_75BB,
            100: self.bring_in_100BB
        }
        chosen_values_of_bring_in = switcher_values_of_bring_in.get(values_of_bring_in, self.bring_in_25BB)

        self.click_on_element(chosen_seats)
        self.click_on_element(chosen_value_of_blinds)
        self.click_on_element(chosen_values_of_bring_in)
        self.click_on_element(self.create_table_button)

        return [number_of_seats, values_of_blinds, values_of_bring_in]


