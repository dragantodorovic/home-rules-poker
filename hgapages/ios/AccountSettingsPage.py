from hgapages.ios import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy
import time
from hgaconfig import LoggingConfig as log


class AccountSettingsPage(HomeScreenPage.HomeScreenPage):

    # Elements on My Profile page
    my_profile_tab = (MobileBy.ID, 'My Profile')
    settings_tab = (MobileBy.ID, 'Settings')
    avatar_view_image = (MobileBy.ID, 'imgViewAvatar')
    nickname_text = (MobileBy.ID, 'lblNickname')

   # Elements on Settings page
    logout_button = (MobileBy.ID, 'titleLogout')


    #----------------------METHODS----------------------

    def logout_user(self):
        time.sleep(4)
        log.logging.info('Player is logging out')
        self.click_on_element(self.avatar_button)
        self.click_on_element(self.settings_tab)
        self.click_on_element(self.logout_button)
