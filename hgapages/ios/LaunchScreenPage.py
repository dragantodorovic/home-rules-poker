from hgaconfig.Constants import *
from hgapages.ios import HomeScreenPage
from appium.webdriver.common.mobileby import MobileBy


class LaunchScreenPage(HomeScreenPage.HomeScreenPage):
    # Elements at launch screen page
    play_as_guest_button = (MobileBy.ID, 'btnPlayAsGuest')
    login_button_launch_screen = (MobileBy.ID, 'btnLogIn')
