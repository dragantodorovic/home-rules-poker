import hgaconfig.BaseConstants as bc


# CONSTANTS FOR ENVIRONMENT iOSNativeExt GSP #

class Constants_iOSnative(bc.BaseConstants):
    # application account credentials
    account_username = "auto1"
    account_username2 = "auto2"
    account_username3 = "auto3"
    account_username4 = "auto4"
    account_username5 = "auto5"
    account_multi1 = "multi1"
    account_multi2 = "multi2"
    account_multi3 = "multi3"
    account_multi4 = "multi4"
    account_multi5 = "multi5"
    account_observer1 = "autoobs1"
    account_observer2 = "autoobs2"
    account_observer3 = "autoobs3"
    account_observer4 = "autoobs4"
    account_password = "qwe123qwe"
    # table number for table with 1 bot
    table_number_1bot = "709"
    # table number for empty table
    table_number_empty = "753"
