
from pathlib import Path


class BaseConstants(object):


    """Constants for both iOS and Android platform"""

    wait_time_long = 35
    # wait time for elements in pre-game play
    wait_time = 6
    # wait time for elements in game play
    wait_time_fast = 0.3
    # time for app to be in background
    time_for_app_in_background = 5
    # Computer home path
    path_home = Path().home()
    # Report path
    # path_of_report = "/Users/dragantodorovic/automation-nativeteam/hgareports/pdf_reports/"
    path_of_report = "%s/home-rules-poker/hgareports/pdf_reports/" % path_home
