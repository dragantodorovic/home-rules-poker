import hgaconfig.BaseConstants as bc


# CONSTANTS FOR ENVIRONMENT iOSNativeExt GSP #

class Constants(bc.BaseConstants):
    account_username = 'autop1'
    account_username2 = "autop2"
    account_username3 = "autop3"
    account_username4 = "autop4"
    account_username5 = "autop5"
    account_multi1 = "multi1"
    account_multi2 = "multi2"
    account_multi3 = "multi3"
    account_multi4 = "multi4"
    account_multi5 = "multi5"
    account_password = "qwe123qwe"
    # table (real money) number for table with 1 bot
    table_number_1bot = "8434735"
    # table (real money) number for empty table
    table_number_empty = "8434736"
    # Observer table URL
    observer_table_url = "http://sios-table.igsoft.local/#/home-rules"

    """
    Real money – QA_1_2 > 8434733 (1 bot)
    Real money - QA_1_2_empty > 8434734 (empty)
    Play money – QA_PM_1_2 > 8434735 (1 bot)
    Play money - QA_PM_1_2_empty	> 8434736 (empty)
    """
