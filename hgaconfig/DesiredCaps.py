from hgaconfig.Constants import *


class DesiredCaps(object):

    # DESIRED CAPABILITIES FOR IPHONE 7 REAL DEVICE
    # desired_caps_ios = {}
    # desired_caps_ios['platformName'] = 'iOS'
    # desired_caps_ios['platformVersion'] = '12.0'
    # desired_caps_ios['deviceName'] = 'iPhone 7'
    # desired_caps_ios['automationName'] = 'XCUITest'
    # desired_caps_ios['app'] = '%s/Documents/ipa file/RevealerSK.ipa' % Constants.path_home
    # desired_caps_ios['udid'] = '4803d3f3883b4aec37c268ce0d92345c1044dc0d'
    # desired_caps_ios['xcodeOrgId'] = '94V79M52B3'
    # desired_caps_ios['xcodeSigningId'] = 'iPhone Developer'
    # desired_caps_ios['fullReset'] = 'True'
    # desired_caps_ios['wdaLocalPort'] = 8100

    # DESIRED CAPABILITIES FOR IPHONE X REAL DEVICE
    # desired_caps_ios2 = {}
    # desired_caps_ios2['platformName'] = 'iOS'
    # desired_caps_ios2['platformVersion'] = '12.1.4'
    # desired_caps_ios2['deviceName'] = 'iPhone X'
    # desired_caps_ios2['automationName'] = 'XCUITest'
    # desired_caps_ios2['app'] = '%s/Documents/ipa file/RevealerSK.ipa' % Constants.path_home
    # desired_caps_ios2['udid'] = '7e36ed7eb701a4a64670c26db438fcae49fa968c'
    # desired_caps_ios2['xcodeOrgId'] = '94V79M52B3'
    # desired_caps_ios2['xcodeSigningId'] = 'iPhone Developer'
    # desired_caps_ios2['fullReset'] = 'True'
    # desired_caps_ios2['wdaLocalPort'] = 8101

    # DESIRED CAPABILITIES FOR IPHONE SIMULATOR
    desired_caps_ios = {}
    desired_caps_ios['platformName'] = 'iOS'
    desired_caps_ios['platformVersion'] = '12.2'
    desired_caps_ios['deviceName'] = 'iPhone 8 Plus'
    desired_caps_ios['automationName'] = 'XCUITest'
    desired_caps_ios['app'] = '%s/Documents/app file/RevealerSK.app' % Constants.path_home
    desired_caps_ios['wdaLocalPort'] = 8100

    # DESIRED CAPABILITIES FOR IPHONE SIMULATOR
    desired_caps_ios2 = {}
    desired_caps_ios2['platformName'] = 'iOS'
    desired_caps_ios2['platformVersion'] = '12.2'
    desired_caps_ios2['deviceName'] = 'iPhone 7 Plus'
    desired_caps_ios2['automationName'] = 'XCUITest'
    desired_caps_ios2['app'] = '%s/Documents/app file/RevealerSK.app' % Constants.path_home
    desired_caps_ios2['wdaLocalPort'] = 8101

    # DESIRED CAPABILITIES FOR IPHONE SIMULATOR
    desired_caps_ios3 = {}
    desired_caps_ios3['platformName'] = 'iOS'
    desired_caps_ios3['platformVersion'] = '12.2'
    desired_caps_ios3['deviceName'] = 'iPhone XS'
    desired_caps_ios3['automationName'] = 'XCUITest'
    desired_caps_ios3['app'] = '%s/Documents/app file/RevealerSK.app' % Constants.path_home
    desired_caps_ios3['wdaLocalPort'] = 8102

    # # DESIRED CAPABILITIES FOR ANDROID SIMULATOR
    desired_caps_android = {}
    desired_caps_android['platformName'] = 'Android'
    desired_caps_android['platformVersion'] = '9.0'
    desired_caps_android['deviceName'] = 'Nexus 5X'
    desired_caps_android['automationName'] = 'uiautomator1'
    desired_caps_android['app'] = '%s/Documents/apk file/app-release.apk' % Constants.path_home
    desired_caps_android['wdaLocalPort'] = 8100
    desired_caps_android['autoGrantPermissions'] = 'true'
    desired_caps_android['autoAcceptAlerts'] = 'true'
    desired_caps_android['avd'] = 'Nexus_5X_API_28'
    desired_caps_android['unicodeKeyboard'] = True

    # # DESIRED CAPABILITIES FOR ANDROID SIMULATOR
    desired_caps_android2 = {}
    desired_caps_android2['platformName'] = 'Android'
    desired_caps_android2['platformVersion'] = '9.0'
    desired_caps_android2['deviceName'] = 'Nexus 6P'
    desired_caps_android2['automationName'] = 'uiautomator1'
    desired_caps_android2['app'] = '%s/Documents/apk file/app-release.apk' % Constants.path_home
    desired_caps_android2['wdaLocalPort'] = 8102
    desired_caps_android2['autoGrantPermissions'] = 'true'
    desired_caps_android2['autoAcceptAlerts'] = 'true'
    desired_caps_android2['avd'] = 'Nexus_6P_API_28'
    desired_caps_android2['unicodeKeyboard'] = True

    # DESIRED CAPABILITIES FOR ANDROID SIMULATOR
    desired_caps_android3 = {}
    desired_caps_android3['platformName'] = 'Android'
    desired_caps_android3['platformVersion'] = '8.1.0'
    desired_caps_android3['deviceName'] = 'Pixel XL'
    desired_caps_android3['automationName'] = 'appium'
    desired_caps_android3['app'] = '%s/Documents/apk file/app-release.apk' % Constants.path_home
    desired_caps_android3['autoGrantPermissions'] = 'true'
    desired_caps_android3['autoAcceptAlerts'] = 'true'
    desired_caps_android3['wdaLocalPort'] = 8101
    desired_caps_android3['avd'] = 'Pixel_XL_API_27'
    # desired_caps_android3['unicodeKeyboard'] = True


    # DESIRED CAPABILITIES FOR ANDROID SIMULATOR
    desired_caps_android4 = {}
    desired_caps_android4['platformName'] = 'Android'
    desired_caps_android4['platformVersion'] = '8.0'
    desired_caps_android4['deviceName'] = 'Pixel 2'
    desired_caps_android4['automationName'] = 'appium'
    desired_caps_android4['app'] = '%s/Documents/apk file/app-release.apk' % Constants.path_home
    desired_caps_android4['wdaLocalPort'] = 8103
    desired_caps_android4['autoGrantPermissions'] = 'true'
    desired_caps_android4['autoAcceptAlerts'] = 'true'
    desired_caps_android4['avd'] = 'Pixel_2_API_26'
    # desired_caps_android4['unicodeKeyboard'] = True


    # DESIRED CAPABILITIES FOR ANDROID REAL DEVICE
    # desired_caps_android = {}
    # desired_caps_android['platformName'] = 'Android'
    # desired_caps_android['platformVersion'] = '8.1'
    # desired_caps_android['deviceName'] = 'HUAWEI P20 Pro'
    # desired_caps_android['app'] = '%s/Documents/apk file/app-release.apk' % Constants.path_home
    # desired_caps_android['autoGrantPermissions'] = 'true'

    #DESIRED CAPABILITIES FOR ANDROID REAL DEVICE 2
    # desired_caps_android2 = {}
    # desired_caps_android2['platformName'] = 'Android'
    # desired_caps_android2['platformVersion'] = '8.0'
    # desired_caps_android2['deviceName'] = 'Galaxy S8'
    # desired_caps_android2['app'] = '%s/Documents/apk file/app-release.apk' % Constants.path_home
    # desired_caps_android2['autoGrantPermissions'] = 'true'

