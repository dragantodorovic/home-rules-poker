import logging


class LoggingConfig(object):
    # loging config
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s:%(levelname)s:%(message)s"
    )
