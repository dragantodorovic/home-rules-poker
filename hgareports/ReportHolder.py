from reportlab.pdfgen import canvas
from reportlab.lib import colors
import datetime
from hgaconfig.Constants import Constants


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver


class ReportHolder(BasePage):

    # it creates report, takes 2 arguments: class_name_is - name of class that is PDF Title, error_message - text
    # shown about error details (optional)
    def run_it(self, class_name_is, error_message=None):
        if error_message is None:
            self.error_message = "Default error message"
        else:
            self.error_message = error_message
        self.class_name_is = class_name_is
        time_of_execution = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        # path_of_report = "/Users/dragantodorovic/automation-nativeteam/hgareports/pdf_reports/"
        c = canvas.Canvas("%s/Report for %s.pdf" % (Constants.path_of_report, class_name_is))
        c.setFillColor(colors.black)
        c.drawString(50, 800, "Test name: %s" % class_name_is)
        c.line(50, 800 - 10, 50 + 450, 800 - 10)
        c.setFillColor(colors.red)
        c.drawString(50, 800 - 30, "Error: %s at time: [%s]" % (self.error_message, time_of_execution))
        self.driver.save_screenshot("%s%s.png" % (Constants.path_of_report, class_name_is))
        c.drawImage("%s%s.png" % (Constants.path_of_report, class_name_is), 50, 800 - 360, 200, 300)
        c.save()
