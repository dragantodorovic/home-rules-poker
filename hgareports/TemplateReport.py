from reportlab.pdfgen import canvas
from reportlab.lib import colors
import datetime
import os


class TemplateReport(object):

    # image for report
    image_for_report = "/Users/nevena.stajic/Documents/Primary_test.jpeg"
    # Image width
    x_image = 200
    # Image height
    y_image = 300
    # First coordinate X
    x = 50
    # First coordinate Y
    y = 800
    # Report name
    report_name = "Report for %s.pdf" % __qualname__
    # Time of execution
    time_of_execution = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    # Font of Report title
    font_title = "Helvetica"
    # Size of font in report tatle
    font_title_size = 14
    # OS
    os_string = os.getcwd()
    print ("Ovde stampam OS: %s" % os_string)

    # Implementation
    # c = canvas.Canvas(report_name)
    # c.setFont(font_title, font_title_size)
    # c.setFillColor(colors.black)
    # c.drawString(x, y, "Test name: %s" % __qualname__)
    # c.setFillColor(colors.red)
    # c.line(x, y-10, x+450, y-10)
    # c.drawString(x, y-30, "Step that follows - FAIL (time: %s)" % time_of_execution)
    # c.drawImage(image_for_report, x, y-360, x_image, y_image)
    # c.save()
