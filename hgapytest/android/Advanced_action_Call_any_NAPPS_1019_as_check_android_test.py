import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgapages.android.JoinGamePage import *
from hgareports.ReportHolder import *
from hgapages.android.GameplayPage import *
from hgapages.android.SettingsPage import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log
import time


class Advanced_action_Call_any_NAPPS_1019_as_check_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android)
        self.driver2 = webdriver.Remote('http://localhost:4723/wd/hub', hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android2)
        self.driver.implicitly_wait(20)
        self.driver2.implicitly_wait(20)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            # Login both users
            account_page = AccountPage(self.driver)
            account_page2 = AccountPage(self.driver2)
            account_page.login_user(Constants.account_multi2)
            account_page2.login_user(Constants.account_multi5)
            # Join game for both users (empty table)
            joingame_page = JoinGamePage(self.driver)
            joingame_page2 = JoinGamePage(self.driver2)
            joingame_page.join_table_and_bring_in(Constants.table_number_empty)
            joingame_page.start_game_in_join_game_flow()
            joingame_page2.join_empty_table()
            # Initialise gameplay for both users
            gameplay_page = GameplayPage(self.driver)
            gameplay_page2 = GameplayPage(self.driver2)
            # Check who is dealer before gameplay
            time.sleep(4)
            [dealer, non_dealer] = gameplay_page.check_who_is_dealer(gameplay_page, gameplay_page2)
            [BALANCE_INFO_FIRST, BALANCE_INFO_SECOND] = gameplay_page.play_for_call_any_as_check_test(dealer, non_dealer)
            log.logging.info('Asserting Balance info after Call any applied as Check')
            assert float(BALANCE_INFO_SECOND) == float(BALANCE_INFO_FIRST)
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise
        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page2 = SettingsPage(self.driver2)
                settings_page.leave_game_player()
                settings_page2.leave_game_player()
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page2 = AccountSettingsPage(self.driver2)
                account_setting_page.logout_user()
                account_setting_page2.logout_user()
            except:
                pass
        time.sleep(6)

    def tearDown(self):
        # end the session
        self.driver.quit()
        self.driver2.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Advanced_action_Call_any_NAPPS_1019_as_check_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
