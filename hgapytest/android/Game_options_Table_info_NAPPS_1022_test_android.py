import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgareports.ReportHolder import *
from hgapages.android.JoinGamePage import *
from hgapages.android.GameplayPage import *
from hgapages.android.SettingsPage import *
from hgapages.android.HostGamePage import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log


class Game_options_Table_info_NAPPS_1022_test_android(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_user()
            joingame_page = JoinGamePage(self.driver)
            joingame_page.join_empty_table()
            log.logging.info(
                'User {} is joined to table {}'.format(Constants.account_username, Constants.table_number_empty))
            settings_page = SettingsPage(self.driver)
            settings_page.click_on_element(settings_page.settings_button)
            log.logging.info('Asserting Game options page')
            game_options_page_is_shown_correctly = settings_page.game_options_page_is_shown_properly()
            settings_page.click_on_element(settings_page.table_info_button)
            log.logging.info('Asserting Table info page')
            table_info_page = HostGamePage(self.driver)
            table_info_page_shown_correctly = table_info_page.table_info_screen_is_shown_properly()
            table_info_page.click_on_element(table_info_page.ok_button)
            log.logging.info('Asserting that player is returned to gameplay')
            settings_page.click_on_element(settings_page.close_X_button)
            gameplay_page = GameplayPage(self.driver)
            assert gameplay_page.label_is_shown(gameplay_page.gameplay_page_text,'Waiting for the next hand to start')
            value = game_options_page_is_shown_correctly and table_info_page_shown_correctly
            assert value
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise
        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page.leave_game_player()
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page.logout_user()


            except:
                pass


    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Game_options_Table_info_NAPPS_1022_test_android)
    unittest.TextTestRunner(verbosity=2).run(suite)
