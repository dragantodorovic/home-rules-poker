import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgapages.android.HostGamePage import *
from hgapages.android.LaunchScreenPage import *

from hgareports.ReportHolder import *
from hgapages.android.SettingsPage import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log


class Player_settings_NAPPS_1021_settings_and_about_page_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android2)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_guest()
            host_a_game_page = HostGamePage(self.driver)
            host_a_game_page.click_on_element(host_a_game_page.avatar_image)
            host_a_game_page.click_on_element(host_a_game_page.setings_tab)
            settings_page = SettingsPage(self.driver)
            log.logging.info('Asserting Settings page ')
            setings_page_is_shown_correctly = settings_page.settings_page_is_shown_properly()
            log.logging.info('Asserting Sound description after turning off sound ')
            settings_page.click_on_element(settings_page.sounds_toggle_button)
            sounds_description = settings_page.label_is_shown(settings_page.sounds_description, 'Sounds are off')
            settings_page.click_on_element(settings_page.about_title)
            log.logging.info('Asserting About page ')
            about_page_is_shown_propery = settings_page.about_page_is_shown_properly()
            settings_page.click_on_element(settings_page.back_arrow_button)
            settings_page.click_on_element(settings_page.logOut_title)
            log.logging.info('Asserting Home Rules Poker page is show after logging out')
            main_screen_is_shown_properly = account_page.button_is_shown(account_page.find_element(account_page.play_as_guest_button), 'Play as Guest')
            value = setings_page_is_shown_correctly and sounds_description and about_page_is_shown_propery and main_screen_is_shown_properly
            self.assertTrue(self, value)
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise

    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Player_settings_NAPPS_1021_settings_and_about_page_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
