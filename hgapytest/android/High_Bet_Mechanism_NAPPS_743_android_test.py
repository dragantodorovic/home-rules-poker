import time
import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgapages.android.JoinGamePage import *
from hgareports.ReportHolder import *
from hgapages.android.GameplayPage import *
from hgapages.android.SettingsPage import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log


class High_Bet_Mechanism_NAPPS_743_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android)
        self.driver.implicitly_wait(10)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_user()
            joingame_page = JoinGamePage(self.driver)
            joingame_page.join_table(buy_in_amount=70)
            log.logging.info('User {} is joined to table {}'.format(Constants.account_username, Constants.table_number_1bot))
            gameplay_page = GameplayPage(self.driver)
            [current_amount, current_button_amount, lowest_picker_value, all_in_picker_value, list_float, desired_list, picker_bb_list] = gameplay_page.play_for_high_bet_mechanism()
            log.logging.info('Asserting multiplied bbs:')
            assert gameplay_page.picker_list_values_snown_properly(list_float, desired_list)
            log.logging.info('Asserting {} and {}'.format(current_button_amount, lowest_picker_value))
            assert current_button_amount == lowest_picker_value
            log.logging.info('Asserting {} and {}'.format(current_amount,all_in_picker_value))
            assert current_amount == all_in_picker_value
            log.logging.info('Asserting picker wheel list')
            assert gameplay_page.picker_list_BB_shown_properly(picker_bb_list)
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise
        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page.leave_game_player()
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page.logout_user()
            except:
                pass
        time.sleep(6)


    def tearDown(self):
        # end the session

        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(High_Bet_Mechanism_NAPPS_743_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
