import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgapages.android.HostGamePage import *

from hgareports.ReportHolder import *
from hgapages.android.SettingsPage import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log


class Player_settings_NAPPS_1021_about_page_links_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android2)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_guest()
            host_a_game_page = HostGamePage(self.driver)
            host_a_game_page.click_on_element(host_a_game_page.avatar_image)
            host_a_game_page.click_on_element(host_a_game_page.setings_tab)
            settings_page = SettingsPage(self.driver)
            settings_page.click_on_element(settings_page.about_title)
            log.logging.info('Opening About RedLark link ')
            settings_page.click_on_element(settings_page.about_redLark)
            time.sleep(2)
            log.logging.info('Back to About page ')
            settings_page.click_on_element(settings_page.back_arrow_button)
            about_page_is_shown_1 = settings_page.about_page_is_shown_properly()
            log.logging.info('Opening Terms and Conditions link ')
            settings_page.click_on_element(settings_page.terms_and_conditions)
            time.sleep(2)
            log.logging.info('Back to About page ')
            settings_page.click_on_element(settings_page.back_arrow_button)
            about_page_is_shown_2 = settings_page.about_page_is_shown_properly()
            log.logging.info('Opening Privacy Policy ')
            settings_page.click_on_element(settings_page.privacy_policy)
            time.sleep(2)
            log.logging.info('Back to About page ')
            settings_page.click_on_element(settings_page.back_arrow_button)
            about_page_is_shown_3 = settings_page.about_page_is_shown_properly()
            value = about_page_is_shown_1 and about_page_is_shown_2 and about_page_is_shown_3
            assert value
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise


    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Player_settings_NAPPS_1021_about_page_links_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
