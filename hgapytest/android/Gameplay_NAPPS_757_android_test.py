import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgapages.android.JoinGamePage import *
from hgareports.ReportHolder import *
from hgapages.android.GameplayPage import *
from hgapages.android.SettingsPage import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log
import  time


class Gameplay_NAPPS_757_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android)
        self.driver.implicitly_wait(24)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_user(Constants.account_username3)
            joingame_page = JoinGamePage(self.driver)
            joingame_page.join_table()
            log.logging.info('User {} is joined to table {}'.format(Constants.account_username3, Constants.table_number_1bot))
            gameplay_page = GameplayPage(self.driver)
            time.sleep(4)
            if gameplay_page.is_dealer() == "true":
                self.dealer_scenario()
                self.non_dealer_scenario()
                self.fold_scenario()
            else:
                self.non_dealer_scenario()
                self.dealer_scenario()
                self.non_dealer_scenario()
                self.fold_scenario()
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise
        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page.leave_game_player()
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page.logout_user()
            except:
                pass
        time.sleep(6)

    def dealer_scenario(self):
        gameplay_page = GameplayPage(self.driver)
        time.sleep(4)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'RAISE')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.call_button_label),'CALL')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.fold_button), 'FOLD')
        gameplay_page.click_on_element(gameplay_page.call_button)
        time.sleep(2)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'BET')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.check_button), 'CHECK')
        gameplay_page.click_on_element(gameplay_page.check_button)
        time.sleep(4)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'BET')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.check_button), 'CHECK')
        gameplay_page.click_on_element(gameplay_page.bet_button)
        time.sleep(4)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'BET')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.check_button), 'CHECK')
        gameplay_page.click_on_element(gameplay_page.check_button)

    def non_dealer_scenario(self):
        gameplay_page = GameplayPage(self.driver)
        time.sleep(4)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'RAISE')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.check_button), 'CHECK')
        gameplay_page.click_on_element(gameplay_page.raise_button)
        time.sleep(2)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'BET')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.check_button), 'CHECK')
        gameplay_page.click_on_element(gameplay_page.check_button)
        time.sleep(2)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'BET')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.check_button), 'CHECK')
        gameplay_page.click_on_element(gameplay_page.bet_button)
        time.sleep(2)
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.raise_bet_button_label_text),'BET')
        assert gameplay_page.button_is_shown(gameplay_page.find_element(gameplay_page.check_button), 'CHECK')
        gameplay_page.click_on_element(gameplay_page.check_button)

    def fold_scenario(self):
        gameplay_page = GameplayPage(self.driver)
        time.sleep(10)
        gameplay_page.click_on_element(gameplay_page.fold_button)
        assert gameplay_page.get_folded_page_text() == "You've folded"
        assert gameplay_page.get_folded_page_subtext() == "Waiting for the next round to start"

    def tearDown(self):
        # end the session
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Gameplay_NAPPS_757_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
