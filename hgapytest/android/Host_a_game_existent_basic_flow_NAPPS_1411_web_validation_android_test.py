import unittest
from appium import webdriver
from selenium import webdriver as wd
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgapages.android.HostGamePage import *
from hgareports.ReportHolder import *
from hgapages.web.WelcomePage import *
from hgapages.web.ObserveGamePage import *
from hgaconfig import LoggingConfig as log


class Host_a_game_existent_basic_flow_NAPPS_1411_web_validation_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android2)
        self.wd = wd.Chrome()
        self.wd.implicitly_wait(18)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_guest()
            host_a_game_page = HostGamePage(self.driver)
            [number_of_seats, values_of_blinds, values_of_bring_in, device_code] = host_a_game_page.host_a_game_existent_flow_web_validation()
            welcome_page = WelcomePage(self.wd)
            welcome_page.open_table(device_code)
            observer_table = ObserverGamePage(self.wd)
            [table_empty_seat_number, table_in_game_seat_number, table_balance_info] = observer_table.pull_data_for_host_a_game_existent_basic_flow()
            log.logging.info('Asserting number of seats on observer table')
            assert number_of_seats == table_empty_seat_number + table_in_game_seat_number
            log.logging.info('Asserting player balance on observer table')
            assert values_of_blinds * values_of_bring_in == table_balance_info
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise

    def tearDown(self):
        # end the session
            self.driver.quit()
            self.wd.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Host_a_game_existent_basic_flow_NAPPS_1411_web_validation_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
