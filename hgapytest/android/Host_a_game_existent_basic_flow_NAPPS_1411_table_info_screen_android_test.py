import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgapages.android.HostGamePage import *
from hgareports.ReportHolder import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig.Constants import *
from hgaconfig import LoggingConfig as log


class Host_a_game_existent_basic_flow_NAPPS_1411_table_info_screen_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_guest()
            host_a_game_page = HostGamePage(self.driver)
            value = host_a_game_page.host_a_game_existent_flow_table_info_screen()
            assert value
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise

    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Host_a_game_existent_basic_flow_NAPPS_1411_table_info_screen_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
