import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.android.AccountPage import *
from hgareports.ReportHolder import *
from hgapages.android.JoinGamePage import *
from hgapages.android.SettingsPage import *
from hgapages.android.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log


class Game_options_Sit_out_NAPPS_1025_android_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_android2)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_user(Constants.account_username4)
            joingame_page = JoinGamePage(self.driver)
            joingame_page.join_empty_table()
            log.logging.info(
                'User {} is joined to table {}'.format(Constants.account_username4, Constants.table_number_empty))
            settings_page = SettingsPage(self.driver)
            settings_page.click_on_element(settings_page.settings_button)
            log.logging.info('Asserting Sit out description text before switch is activated')
            assert settings_page.label_is_shown(settings_page.sit_out_description, "Take a break for new hands")
            settings_page.click_on_element(settings_page.sit_out_switch)
            log.logging.info('Asserting Sit out description text after switch is activated')
            assert settings_page.label_is_shown(settings_page.sit_out_description,
                                                "Take a break for new hands")
        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise
        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page.click_on_element(settings_page.leave_game_button)
                settings_page.click_on_element(settings_page.leave_table_popup_button)
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page.logout_user()


            except:
                pass


    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Game_options_Sit_out_NAPPS_1025_android_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
