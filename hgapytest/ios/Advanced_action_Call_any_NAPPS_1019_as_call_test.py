import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.ios.AccountPage import *
from hgapages.ios.JoinGamePage import *
from hgareports.ReportHolder import *
from hgapages.ios.GameplayPage import *
from hgapages.ios.SettingsPage import *
from hgapages.ios.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log
import time


class Advanced_action_Call_any_NAPPS_1019_as_call_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_ios)
        self.driver2 = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_ios2)
        self.driver.implicitly_wait(20)
        self.driver2.implicitly_wait(20)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            # Login both users
            account_page = AccountPage(self.driver)
            account_page2 = AccountPage(self.driver2)
            account_page.login_user(Constants.account_multi2)
            account_page2.login_user(Constants.account_multi5)

            # Join game for both users (empty table)
            joingame_page = JoinGamePage(self.driver)
            joingame_page2 = JoinGamePage(self.driver2)
            joingame_page.join_table_and_bring_in(Constants.table_number_empty)

            # This is amount that will player have at start of the game
            MINIMUM_VALUE_FOR_TABLE = joingame_page.get_minimum_value_text()
            joingame_page.start_game_in_join_game_flow()
            joingame_page2.join_empty_table()
            # Initialise gameplay for both users
            gameplay_page = GameplayPage(self.driver)
            gameplay_page2 = GameplayPage(self.driver2)

            # Check who is dealer before gameplay
            time.sleep(4)
            [dealer, non_dealer]= gameplay_page.check_who_is_dealer(gameplay_page, gameplay_page2)
            BALANCE_INFO_FIRST = dealer.get_current_amount()
            SB_AMOUNT = dealer.get_sb_value()
            # Asserting that player have balance that is TABLE BALANCE (table minimum is selected)
            # minus Small Blind (dealer puts SB on table)
            assert float(BALANCE_INFO_FIRST) == float(MINIMUM_VALUE_FOR_TABLE) - float(SB_AMOUNT)
            [AMOUNT_OF_RAISE, BALANCE_INFO_SECOND] = gameplay_page.play_for_as_call_test(dealer, non_dealer)
            log.logging.info('Asserting Balance info before first hand')
            assert float(BALANCE_INFO_SECOND) == float(MINIMUM_VALUE_FOR_TABLE) - float(AMOUNT_OF_RAISE)

        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise
        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page2 = SettingsPage(self.driver2)
                settings_page.leave_game_player()
                settings_page2.leave_game_player()
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page2 = AccountSettingsPage(self.driver2)
                account_setting_page.logout_user()
                account_setting_page2.logout_user()
            except:
                pass
        time.sleep(6)

    def tearDown(self):
        # end the session
        self.driver.quit()
        self.driver2.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Advanced_action_Call_any_NAPPS_1019_as_call_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
