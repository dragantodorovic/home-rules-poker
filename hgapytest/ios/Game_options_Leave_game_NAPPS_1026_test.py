import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.ios.AccountPage import *
from hgareports.ReportHolder import *
from hgapages.ios.JoinGamePage import *
from hgapages.ios.GameplayPage import *
from hgapages.ios.SettingsPage import *
from hgapages.ios.HostGamePage import *
from hgapages.ios.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log
from hgapages.ios.HomeScreenPage import  *


class Game_options_Leave_game_NAPPS_1026_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_ios2)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_user()
            joingame_page = JoinGamePage(self.driver)
            joingame_page.join_empty_table()
            log.logging.info('User {} is joined to table {}'.format(Constants.account_username, Constants.table_number_empty))
            settings_page = SettingsPage(self.driver)
            settings_page.click_on_element(settings_page.settings_button)
            settings_page.click_on_element(settings_page.leave_game_button)
            leave_game_page_is_shown_properly = settings_page.leave_game_page_is_shown_properly()
            log.logging.info('Clicking on Cancel button')
            settings_page.click_on_element(settings_page.leave_game_pop_up_cancel)
            gameplay_page = GameplayPage(self.driver)
            log.logging.info('Asserting gameplay page ')
            gameplay_page_is_shown_properly = gameplay_page.label_is_shown(gameplay_page.gameplay_page_text, 'Waiting for the next hand to start')
            settings_page.click_on_element(settings_page.settings_button)
            log.logging.info('Clicking on Leave game button')
            settings_page.click_on_element(settings_page.leave_game_button)
            settings_page.click_on_element(settings_page.leave_table_popup_button)
            home_screen_page = HomeScreenPage(self.driver)
            log.logging.info('Asserting Home screen page ')
            host_a_game_button_is_shown = home_screen_page.button_is_shown(home_screen_page.find_element(home_screen_page.host_a_game_button), 'Host a Game')
            value = leave_game_page_is_shown_properly and gameplay_page_is_shown_properly and host_a_game_button_is_shown
            assert value

        except Exception as e:
            report = ReportHolder(self.driver)
            report.run_it(self.class_name_is, type(e).__name__)
            raise
        finally:
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page.logout_user()
            except:
                pass


    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Game_options_Leave_game_NAPPS_1026_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
