import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.ios.AccountPage import *
from hgareports.ReportHolder import *
from hgapages.ios.JoinGamePage import *
from hgapages.ios.GameplayPage import *
from hgapages.ios.SettingsPage import *
from hgapages.ios.HostGamePage import *
from hgapages.ios.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log
from hgapages.ios.HomeScreenPage import  *


class Game_options_Leaderboard_NAPPS_1024_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_ios)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_user()
            joingame_page = JoinGamePage(self.driver)
            joingame_page.join_table_and_bring_in(table_number=Constants.table_number_empty)
            amount = joingame_page.get_minimum_value_text()
            joingame_page.start_game_in_join_game_flow()
            log.logging.info('User {} is joined to table {}'.format(Constants.account_username, Constants.table_number_empty))
            settings_page = SettingsPage(self.driver)
            settings_page.click_on_element(settings_page.settings_button)
            settings_page.click_on_element(settings_page.leaderboard_button)
            leaderboared_page_is_shown_propelry = settings_page.leaderboard_page_is_shown_properly()
            log.logging.info('Asserting player amount')
            amount_is_shown_properly = settings_page.label_is_shown(settings_page.player_amount,amount)
            value = amount_is_shown_properly and leaderboared_page_is_shown_propelry
            assert value
            settings_page.click_on_element(settings_page.back_arrow_button)
            settings_page.click_on_element(settings_page.leaderboard_button)
            settings_page.click_on_element(settings_page.close_X_button)
            gameplay_page = GameplayPage(self.driver)
            assert gameplay_page.label_is_shown(gameplay_page.gameplay_page_text,'Waiting for the next hand to start')

        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page.leave_game_player()
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page.logout_user()
            except:
                pass

    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Game_options_Leaderboard_NAPPS_1024_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
