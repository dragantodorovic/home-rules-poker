import unittest
from appium import webdriver
import hgaconfig.DesiredCaps
from hgapages.ios.AccountPage import *
from hgareports.ReportHolder import *
from hgapages.ios.JoinGamePage import *
from hgapages.ios.GameplayPage import *
from hgapages.ios.SettingsPage import *
from hgapages.ios.HostGamePage import *
from hgapages.ios.AccountSettingsPage import *
from hgaconfig import LoggingConfig as log
from hgapages.ios.HomeScreenPage import  *


class Gameplay_Accelerators_3BB_no_previous_bet_NAPPS_1823_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',hgaconfig.DesiredCaps.DesiredCaps.desired_caps_ios2)

    class_name_is = __qualname__

    def test_activity(self):
        time.sleep(3)
        try:
            account_page = AccountPage(self.driver)
            account_page.login_user()
            joingame_page = JoinGamePage(self.driver)
            joingame_page.join_table()
            log.logging.info('User {} is joined to table {}'.format(Constants.account_username, Constants.table_number_1bot))
            gameplay_page = GameplayPage(self.driver)
            # big_blind_value = float (gameplay_page.get_bb_value())
            # small_blind_value = float (gameplay_page.get_sb_value())
            # difference = big_blind_value - small_blind_value
            value = gameplay_page.accelerator_buttons_are_shown_properly()
            assert value
            raise_button_value_before = float (gameplay_page.get_amount_from_the_button(gameplay_page.find_element(gameplay_page.raise_button)))
            log.logging.info('Raise button value before: {}'.format(raise_button_value_before))
            gameplay_page.click_on_element(gameplay_page.raise_3bb_button)
            raise_button_value_after = gameplay_page.get_amount_from_the_button(gameplay_page.find_element(gameplay_page.raise_button))
            log.logging.info('Raise button value after: {}'.format(raise_button_value_after))
            # total = 3 * big_blind_value + difference
            # log.logging.info('Total:'.format(raise_button_value_after))
            log.logging.info('Asserting raise button value is changed')
            assert raise_button_value_before != raise_button_value_after

        finally:
            try:
                settings_page = SettingsPage(self.driver)
                settings_page.leave_game_player()
            except:
                pass
            try:
                account_setting_page = AccountSettingsPage(self.driver)
                account_setting_page.logout_user()
            except:
                pass

    def tearDown(self):
        # end the session
            self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Gameplay_Accelerators_3BB_no_previous_bet_NAPPS_1823_test)
    unittest.TextTestRunner(verbosity=2).run(suite)
